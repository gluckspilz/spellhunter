/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert, Sebastien Benard
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

import ui.Components;
import ui.Colors;
import hxd.Key;
import haxe.ds.Option;


class SpellSlotUiComp extends ButtonComp {

	var getCaster : () -> spl.Caster;
	var root : h2d.Object;
	var id : Int;

	var control : tools.CustomControl;

	static public function tileSize() {
		return Std.int(32 * 2 * Const.UI_SCALE);
	}

	static public function getTiles(id) {
		var w = tileSize();
		var h = tileSize();

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		var hoverTile = new Array();
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);

		if (id == 0) {
			baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot1NotSelected)));
			hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot1Selected)));
			activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot1Active)));
		} else if (id == 1) {
			baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot2NotSelected)));
			hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot2Selected)));
			activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot2Active)));
		} else if (id == 2) {
			baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot3NotSelected)));
			hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot3Selected)));
			activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot3Active)));
		} else if (id == 3) {
			baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot4NotSelected)));
			hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot4Selected)));
			activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Slot4Active)));
		}

		switch (Game.ME.spellCaster[id].formula) {
			case None:
			case Some (s):
				spl.Token.LangToSprite.addSpellSprite(baseTile, s, 8, 8);
				spl.Token.LangToSprite.addSpellSprite(activeTile, s, 8, 8);
		}

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function new( id : Int, label : String, root : h2d.Object, getCaster : () -> spl.Caster, control : tools.CustomControl, ?parent ) {
		super(getTiles(id), parent);
		initComponent();

		this.id = id;
		this.root = root;
		this.getCaster = getCaster;
		this.label = label;
		this.control = control;

		onOver = function() {
			showSpell();
		};

		onOut = function() {
			root.removeChildren();
		};
	}

	function showSpell() {
		var allChoices = new Array();
		for (w in Game.ME.spellWords.iterator())
			allChoices.push(w);

		var o = { addDropdown : None
			, addToDropdownLayer : None
			, allChoices : allChoices
			, newWords : new Map()
			, control : control
			, repl : Main.ME.repl
		};

		var s = spl.Selector.ME.getASpellComponent(o, getCaster().formula);

		root.addChild(s);
		s.redraw();
	}

	public function update() {
		var c = getCaster();
		visible = !c.isLocked;

		var t = getTiles(id);
		activeTile = t.active;
		hoverTile = t.hover;
		baseTile = t.base;

		syncScale();

		if (dom.active)
			showActive();
		else if (dom.hover)
			showHover();
		else
			showBase();
	}
}

class Hud extends dn.Process {
	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;
	public var level(get,never) : Level; inline function get_level() return Game.ME.level;

	var ca : dn.legacy.Controller.ControllerAccess;
	var control : tools.CustomControl;
	var invalidated = true;

	var spellDisplay : h2d.Object;
	var spellSlotCtn : ContainerComp;
	var spellSlots : Array<SpellSlotUiComp> = new Array();

	public function new() {
		super(Game.ME);

		ca = Main.ME.controller.createAccess("ui");
		control = new tools.CustomControl(ca);
		createRoot(Boot.ME.uiRoot);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

		spellDisplay = new h2d.Object(root);

		spellSlotCtn = new ContainerComp(root);
		spellSlotCtn.layout = Horizontal;
		spellSlotCtn.horizontalAlign = Left;
		spellSlotCtn.verticalAlign = Middle;

		onResize();
	}

	override function onResize() {
		super.onResize();

		spellDisplay.x = Std.int( w()*0.2 );
		spellDisplay.y = Std.int( h()*0.5 );

		var size = SpellSlotUiComp.tileSize();

		spellSlotCtn.x = Std.int( w()*0.35 );
		spellSlotCtn.y = Std.int( h() - size - h() * 0.005 );

		for (s in spellSlots) {
			s.resize(size, size);
		}
	}

	public inline function invalidate() invalidated = true;

	function render() {
		for (s in spellSlots)
			s.update();
	}

	override function postUpdate() {
		super.postUpdate();

		if( invalidated ) {
			invalidated = false;
			render();
		}
	}

	public function makeSlot(label, sc) {
		var s =	new SpellSlotUiComp(spellSlots.length, label, spellDisplay, sc, control, spellSlotCtn);
		spellSlots.push(s);
	}
}
