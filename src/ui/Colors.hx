/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

typedef ColorData = {
	var active : Int;
	var base : Int;
	var hover : Int;
	var text : Int;
}

class Colors {
	static public final defaultColor = { active : 0x111111, base : 0x222222, hover : 0x444444, text : 0xffffff };

	static public final slot1 = { active : 0x426951, base : 0x92e8b4, hover : 0xc0e8d0, text : 0x222222 };
	static public final slot2 = { active : 0xd3cf87, base : 0xfffbad, hover : 0xf7f5cd, text : 0x222222 };
	static public final slot3 = { active : 0xb58d72, base : 0xe8b692, hover : 0xe8d1c0, text : 0x222222 };
	static public final slot4 = { active : 0xc178bc, base : 0xff9ef8, hover : 0xfcc7f8, text : 0x222222 };

	static public final slotColors = [ slot1, slot2, slot3, slot4 ];
}
