/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import h2d.Sprite;
import dn.heaps.HParticle;
import dn.Tweenie;


class Fx extends dn.Process {
	public var pool : ParticlePool;

	public var bgAddSb    : h2d.SpriteBatch;
	public var bgNormalSb    : h2d.SpriteBatch;
	public var topAddSb       : h2d.SpriteBatch;
	public var topNormalSb    : h2d.SpriteBatch;

	var discard : h2d.SpriteBatch;

	var game(get,never) : Game; inline function get_game() return Game.ME;

	public function new() {
		super(Game.ME);

		pool = new ParticlePool(Assets.sprites.tile, 2048, Const.FPS);

		bgAddSb = new h2d.SpriteBatch(Assets.sprites.tile);
		game.scroller.add(bgAddSb, Const.DP_FX_BG);
		bgAddSb.blendMode = Add;
		bgAddSb.hasRotationScale = true;

		bgNormalSb = new h2d.SpriteBatch(Assets.sprites.tile);
		game.scroller.add(bgNormalSb, Const.DP_FX_BG);
		bgNormalSb.hasRotationScale = true;

		topNormalSb = new h2d.SpriteBatch(Assets.sprites.tile);
		game.scroller.add(topNormalSb, Const.DP_TOP);
		topNormalSb.hasRotationScale = true;

		topAddSb = new h2d.SpriteBatch(Assets.sprites.tile);
		game.scroller.add(topAddSb, Const.DP_FX_FRONT);
		topAddSb.blendMode = Add;
		topAddSb.hasRotationScale = true;


		discard = new h2d.SpriteBatch(Assets.sprites.tile);
	}

	override public function onDispose() {
		super.onDispose();

		pool.dispose();
		bgAddSb.remove();
		bgNormalSb.remove();
		topAddSb.remove();
		topNormalSb.remove();
	}

	public function clear() {
		pool.clear();
	}

	override public function preUpdate() {
		super.preUpdate();

		discard.clear();
	}

	public inline function allocTopAdd(t:h2d.Tile, x:Float, y:Float) : HParticle {
		if (Settings.particles)
			return pool.alloc(topAddSb, t, x, y);
		else
			return pool.alloc(discard, t, x, y);
	}

	public inline function allocTopNormal(t:h2d.Tile, x:Float, y:Float) : HParticle {
		if (Settings.particles)
			return pool.alloc(topNormalSb, t,x,y);
		else
			return pool.alloc(discard, t, x, y);
	}

	public inline function allocBgAdd(t:h2d.Tile, x:Float, y:Float) : HParticle {
		if (Settings.particles)
			return pool.alloc(bgAddSb, t,x,y);
		else
			return pool.alloc(discard, t, x, y);
	}

	public inline function allocBgNormal(t:h2d.Tile, x:Float, y:Float) : HParticle {
		if (Settings.particles)
			return pool.alloc(bgNormalSb, t,x,y);
		else
			return pool.alloc(discard, t, x, y);
	}

	public inline function getTile(id:String) : h2d.Tile {
		return Assets.sprites.getTileRandom(id);
	}

	public function killAll() {
		pool.clear();
	}

	public function markerEntity(e:Entity, ?c=0xFF00FF, ?short=false) {
		#if debug
		if( e==null )
			return;

		markerCase(e.cx, e.cy, short?0.03:3, c);
		#end
	}

	public function markerCase(cx:Int, cy:Int, ?sec=3.0, ?c=0xFF00FF) {
		#if debug
		var p = allocTopAdd(getTile("fxCircle"), (cx+0.5)*Const.GRID, (cy+0.5)*Const.GRID);
		p.setFadeS(1, 0, 0.06);
		p.colorize(c);
		p.lifeS = sec;

		var p = allocTopAdd(getTile("pixel"), (cx+0.5)*Const.GRID, (cy+0.5)*Const.GRID);
		p.setFadeS(1, 0, 0.06);
		p.colorize(c);
		p.setScale(2);
		p.lifeS = sec;
		#end
	}

	public function markerFree(x:Float, y:Float, ?sec=3.0, ?c=0xFF00FF) {
		#if debug
		var p = allocTopAdd(getTile("fxDot"), x,y);
		p.setCenterRatio(0.5,0.5);
		p.setFadeS(1, 0, 0.06);
		p.colorize(c);
		p.setScale(3);
		p.lifeS = sec;
		#end
	}

	public function markerText(cx:Int, cy:Int, txt:String, ?t=1.0) {
		#if debug
		var tf = new h2d.Text(Assets.fontTiny, topNormalSb);
		tf.text = txt;

		var p = allocTopAdd(getTile("fxCircle"), (cx+0.5)*Const.GRID, (cy+0.5)*Const.GRID);
		p.colorize(0x0080FF);
		p.alpha = 0.6;
		p.lifeS = 0.3;
		p.fadeOutSpeed = 0.4;
		p.onKill = tf.remove;

		tf.setPosition(p.x-tf.textWidth*0.5, p.y-tf.textHeight*0.5);
		#end
	}

	public function flashBangS(c:UInt, a:Float, ?t=0.1) {
		var e = new h2d.Bitmap(h2d.Tile.fromColor(c,1,1,a));
		game.root.add(e, Const.DP_FX_FRONT);
		e.scaleX = game.w();
		e.scaleY = game.h();
		e.blendMode = Add;
		game.tw.createS(e.alpha, 0, t).end( function() {
			e.remove();
		});
	}

	public function firefly(x, y) {
		var n = 5;
		for( i in 0...n) {
			var x = (x + Math.random()) * Const.GRID;
			var y = (y + Math.random()) * Const.GRID;

			var p = allocTopNormal(getTile("Firefly"), x, y);

			var pos = new h3d.Vector(x, y, Const.GRID/4, 0);
			var lightSource = new Lights.LightSource(pos, 0xffffff);

			p.dx = 0;
			p.dy = 0;
			p.lifeS = 4;
			p.setFadeS(1, 1, 1);
			p.delayS = Math.random()*10;
			p.onStart = function() {
				pos.z = 0;
				Game.ME.lights.addLightSource(lightSource);
			}
			p.onUpdate = function(p) {
				pos.z = p.alpha * Const.GRID;
				lightSource.x = p.x;
				lightSource.y = p.y;
			}
			p.onKill = function () {
				Game.ME.lights.removeLightSource(lightSource);
			}
		}
	}

	public function groupFirefly(x, y, t) {
		var n = 10;
		var color = 0xa85959;
		for( i in 0...n) {
			var radius = Const.GRID/2;
			var r = radius * (Math.random() - 0.5);
			var angle = Math.random() * 2 * Math.PI;
			var nx = r * Math.cos(angle);
			var ny = r * Math.sin(angle);
			var p = allocTopNormal(getTile("Firefly"), x + nx, y + ny);
			p.colorize(color);

			var pos = new h3d.Vector(x + nx, y + ny, Const.GRID/4, 0);
			var lightSource = new Lights.LightSource(pos, color);

			p.lifeS = t;

			p.onStart = function() {
				Game.ME.lights.addLightSource(lightSource);
			}
			p.onUpdate = function(p) {
				lightSource.x = p.x;
				lightSource.y = p.y;
			}

			p.onKill = function () {
				Game.ME.lights.removeLightSource(lightSource);
			}
		}
	}

	public function spreadFirefly(x, y, s) {
		var n = 10;
		var color = 0xa85959;
		for( i in 0...n) {
			var p = allocTopNormal(getTile("Firefly"), x, y);
			var speed = 0.2;
			var angle = Math.random() * 2 * Math.PI;
			var dx = speed * Math.cos(angle);
			var dy = speed * Math.sin(angle);

			var pos = new h3d.Vector(x, y, Const.GRID/4, 0);
			var lightSource = new Lights.LightSource(pos, color);

			p.colorize(color);
			p.lifeS = 0;
			p.setFadeS(1, 0, 1);
			p.onUpdate = function(p) {
				var speed = 1;
				var angle = Math.random() * 2 * Math.PI;
				p.dx = dx + speed * Math.cos(angle);
				p.dy = dy + speed * Math.sin(angle);
				if (p.scaleX <= 3) {
					p.setScale(3);
					p.scaleMul = 1.;
				}

				lightSource.x = p.x;
				lightSource.y = p.y;
			}
			p.scaleMul = s;

			p.onStart = function() {
				Game.ME.lights.addLightSource(lightSource);
			}
			p.onKill = function () {
				Game.ME.lights.removeLightSource(lightSource);
			}
		}
	}

	public function plouf(x, y, color) {
		var n = 10;
		for( i in 0...n) {
			var p = allocTopNormal(getTile("Firefly"), (x + Math.random()) * Const.GRID, (y + Math.random()) * Const.GRID);

			var centerX = (x + 0.5) * Const.GRID;
			var centerY = (y + 0.5) * Const.GRID;

			var dist = M.dist(p.x, p.y, centerX, centerY);
			var ang = Math.atan2(p.y-centerY, p.x-centerX);
			var force = 1;
			p.dx = Math.cos(ang) * force;
			p.dy = Math.sin(ang) * force;
			p.lifeS = 0.5;
			p.setFadeS(1, 0, 0.5);
			p.setScale(3);
			p.colorize(color);
		}
	}

	public function brokenBits(x, y, dx, dy, frict, color) {
		var n = 10;
		for( i in 0...n) {
			var p = allocTopNormal(getTile("Firefly"), (x + Math.random()) * Const.GRID, (y + Math.random()) * Const.GRID);

			p.dx = dx;
			p.dy = dy;
			p.lifeS = 0.1;
			p.setFadeS(1, 0, 0.75);
			p.setScale(3);
			p.colorize(color);
			p.frict = frict;
		}
	}

	public function bleed(x, y, dx : Float) {
		var speed = 1.;
		var n = 10;
		for( i in 0...n) {
			var p = allocTopNormal(getTile("Firefly"), x, y);

			p.dx = 5 * dx * speed;
			p.dy = -speed * Math.sin(Math.random() * Math.PI/4 + Math.PI/4);
			p.lifeS = 0.1;
			p.setFadeS(1, 0, 0.75);
			p.setScale(3);
			p.colorize(0x990000);
			p.frict = 0.98;
			p.delayS = i==0 ? 0 : rnd(0,0.1);
			p.onUpdate =
				(p) ->
				if (p.y < y + Const.GRID / 5)
					p.dy += Const.GRAVITY + this.tmod;
				else {
					p.dy = 0;
					p.dx = 0;
				}
		}
	}

	public function evaporate(x, y, color) {
		var n = 15;
		for( i in 0...n) {
			var p = allocTopNormal(getTile("Firefly"), (x + Math.random()) * Const.GRID, (y + Math.random()) * Const.GRID);

			p.dx = 0;
			p.dy = -0.75;
			p.lifeS = 0.1;
			p.setFadeS(1, 0, 0.75);
			p.setScale(3);
			p.colorize(color);
			p.delayS = Math.random();
		}
	}

	public function aspirate(x, y, color, s) {
		var n = 10;
		for( i in 0...n) {
			var p = allocTopNormal(getTile("Firefly"), (x + (Math.random()*1.5-0.75) * Const.GRID), (y + (Math.random()*1.5-0.75) * Const.GRID));

			var dist = M.dist(p.x, p.y, x, y);
			var ang = Math.atan2(p.y-y, p.x-x);
			var force = 1;
			p.dx = -Math.cos(ang) * force;
			p.dy = -Math.sin(ang) * force;

			p.lifeS = 0.1;
			p.setFadeS(1, 0, 0.25);
			p.setScale(3);
			p.colorize(color);
			p.scaleMul = s;
		}
	}

	public function generateOnShape(gen : (Float, Float) -> Void, s : tools.Geometry.Shape) {
		switch (s) {
			case Circle (x, y, r):
				var n = 10;
				for( i in 0...n) {
					var radius = Math.random() * r;
					var angle = Math.random() * 2 * Math.PI;
					var nx = x + radius * Math.cos(angle);
					var ny = y + radius * Math.sin(angle);

					gen(nx, ny);
				}

			case Segment (x1, y1, x2, y2):
				var dx = x2 - x1;
				var dy = y2 - y1;

				var b = rnd(0,1);
				var x = x1 + b * dx;
				var y = y1 + b * dy;

				gen(x, y);
		}
	}


	public function snow(s : tools.Geometry.Shape) {
		var gen = function (x : Float, y : Float) {
			var p = allocTopNormal(getTile("SnowFlake"), x, y - Const.GRID);

			p.dx = 0;
			p.dy = 0.5;

			p.lifeS = 0.1;
			p.setFadeS(1, 0, 0.75);
		}

		generateOnShape(gen, s);
	}

	public function fire(s : tools.Geometry.Shape, shouldExclude : (Float, Float) -> Bool) {
		var colors = [ 0xcf3a26, 0xee5b41 ];

		var gen = function (x : Float, y : Float) {
			if (shouldExclude(x, y))
				return;

			var p = allocBgNormal(getTile("fxFlame"), x, y);

			p.colorize(colors[M.rand(colors.length)]);

			p.setFadeS(1, 0.1, 0.5);
			p.rotation = -rnd(0.1,0.2);
			p.dy = -rnd(0.4, 1.3);
			p.frict = rnd(0.94, 0.96);
			p.lifeS = rnd(0.2,0.3);
			p.delayS = rnd(0,0.1);

			p.dx = 0;
			p.dy = -0.1;
		}

		generateOnShape(gen, s);
	}

	public function melt(x, y) {
		var n = 10;
		for( i in 0...n) {
			var p = allocTopNormal(getTile("fxDrop"), (x + Math.random()) * Const.GRID, (y + Math.random()) * Const.GRID);

			p.setFadeS(1, 0.1, 0.5);
			p.dy = -rnd(0.4, 1.3);
			p.frict = rnd(0.94, 0.96);
			p.lifeS = rnd(0.2,0.3);
			p.setScale(2);
			p.delayS = i==0 ? 0 : rnd(0,0.1);

			p.dx = 0;
			p.dy = 0.1;
		}
	}

	override function update() {
		super.update();

		pool.update(game.tmod);
	}
}
