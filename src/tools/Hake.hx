/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;

#if( js )
import js.Browser.document;
#end

typedef HakeOption = {
	var label : String;
	var select : () -> Void;
}

typedef Command = {
	var description : String;
	var command : () -> Void;
}

class Hake extends dn.Process {

	var lastLog : String = "";
	var lastMenu : Array<HakeOption> = null;

	var ca : dn.legacy.Controller.ControllerAccess;
	var commands : Map<String, Command> = new Map();

	public function new(){
		super(Main.ME);
#if( js )
		initSubmit();
#end
		var help = function () {
			var commandsStrings = [ for (c in commands.keyValueIterator()) c.key + " -- " + c.value.description ];
			log(commandsStrings.join("\n"));
		};
		addCommand("help", "Displays this menu.", help);

		var sightless = function () {
			Settings.sightless = true;
			Settings.discrete = Settings.sightless;
			Settings.assistedAiming = Settings.sightless;
			setVisible();
#if( js )
			document.querySelector("#webgl").focus();
#end
			if (lastMenu == null)
				return;

			menu(lastMenu);
		}
		addCommand("sightless", "Activate the sightless mode.", sightless);
	}

	override function onDispose() {
		super.onDispose();
	}

	public function log(s : String) {
		if (s != lastLog) {
			lastLog = s;
#if( js )
			var p = document.createSpanElement();
			p.innerText = s;

			document.querySelector(".container").appendChild(p);

			var p = document.createBRElement();
			document.querySelector(".container").appendChild(p);

			var b = document.querySelector("#command");
			if (Settings.sightless) {
				b.scrollIntoView();
			}
#end
		}
	}

	private function getIndex() {
		var b : js.html.InputElement = cast document.querySelector("#command");

		var v = b.value;
		b.value = "";
		return Std.parseInt(v);
	}

	public function addCommand (name : String, descr : String, command : () -> Void) {
		var c = {
			description : descr,
			command : command,
		};
		commands.set(name, c);
	}

#if( js )
	private function initSubmit() {
			var b = document.querySelector("#form");
			b.onsubmit = function () {
				var b : js.html.InputElement = cast document.querySelector("#command");

				var v = b.value;
				b.value = "";

				initSubmit();

				var c = commands.get(v);

				if (c == null)
					return;

				c.command();
				Main.ME.controller.suspend(0.25);
			}
	}
#end

	public function menu(items : Array<HakeOption>) {
		lastMenu = items;
#if( js )
		log("==========");
		for (i in 0...items.length) {
			log((i + 1) + " " + items[i].label);
		}

		var b = document.querySelector("#form");
		b.onsubmit = function () {
			var i = getIndex();
			if (i == null || i > items.length)
				return;

			document.querySelector("#webgl").focus();
			ca = null;
			initSubmit();
			items[i-1].select();
			Main.ME.controller.suspend(0.25);
			lastMenu = null;
		};
		log("==========");

		if (Settings.sightless) {
			var b = document.querySelector("#command");
			b.scrollIntoView();
			b.focus();
		}
#end
	}

	public function setVisible() {
		if (Settings.sightless) {
			Main.ME.gfh.resumeGame();
		}
	}
}
