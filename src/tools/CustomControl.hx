/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;
import hxd.Key;

enum Action {
	Up;
	Down;
	Left;
	Right;
	Select;
	Exit;
	Interact;
	Slot1;
	Slot2;
	Slot3;
	Slot4;
	Map;
	AimUp;
	AimDown;
	AimLeft;
	AimRight;
	SelectAim;
}

typedef Input = {
	var code : Int;
	var type : dn.legacy.Controller.Mode;
}

typedef KeyBinding = {
	var modifiers : Array<Input>;
	var key : Input;
	var protected : Bool;
}

class CustomControl  {
	static private var conflicts = new Map();
	static private var candidates = new Map();
	static public var bindings(default, null) = new Map();

	var ca : dn.legacy.Controller.ControllerAccess;

	public function new(ca) {
		this.ca = ca;
	}

	static public function key(k : Int) {
		return { code : k, type : dn.legacy.Controller.Mode.Keyboard };
	}


	static public function pad(c : Int) {
		return { code : c, type : dn.legacy.Controller.Mode.Pad };
	}

	static public function init() {
		addBinding(Up, { modifiers : new Array(), key : key(Key.UP), protected : true });
		addBinding(Up, { modifiers : new Array(), key : key(Key.Z), protected : false });
		addBinding(Up, { modifiers : new Array(), key : key(Key.W), protected : false });

		addBinding(Down, { modifiers : new Array(), key : key(Key.DOWN), protected : true });
		addBinding(Down, { modifiers : new Array(), key : key(Key.S), protected : false });

		addBinding(Left, { modifiers : new Array(), key : key(Key.LEFT), protected : true });
		addBinding(Left, { modifiers : new Array(), key : key(Key.A), protected : false });
		addBinding(Left, { modifiers : new Array(), key : key(Key.Q), protected : false });

		addBinding(Right, { modifiers : new Array(), key : key(Key.RIGHT), protected : true });
		addBinding(Right, { modifiers : new Array(), key : key(Key.D), protected : false });

		addBinding(Select, { modifiers : new Array(), key : key(Key.ENTER), protected : true });

		addBinding(Exit, { modifiers : new Array(), key : key(Key.ESCAPE), protected : true });

		addBinding(Interact, { modifiers : new Array(), key : key(Key.E), protected : false });

		addBinding(Slot1, { modifiers : new Array(), key : key(Key.NUMBER_1), protected : false });
		addBinding(Slot1, { modifiers : new Array(), key : key(Key.MOUSE_LEFT), protected : false });

		addBinding(Slot2, { modifiers : new Array(), key : key(Key.NUMBER_2), protected : false });
		addBinding(Slot2, { modifiers : new Array(), key : key(Key.MOUSE_RIGHT), protected : false });

		addBinding(Slot3, { modifiers : new Array(), key : key(Key.NUMBER_3), protected : false });
		addBinding(Slot3, { modifiers : new Array(), key : key(Key.MOUSE_MIDDLE), protected : false });

		addBinding(Slot4, { modifiers : new Array(), key : key(Key.NUMBER_4), protected : false });

		addBinding(Map, { modifiers : new Array(), key : key(Key.M), protected : false });

		addBinding(AimUp, { modifiers : [key(Key.CTRL)], key : key(Key.Z), protected : false });
		addBinding(AimUp, { modifiers : [key(Key.CTRL)], key : key(Key.W), protected : false });
		addBinding(AimUp, { modifiers : [key(Key.CTRL)], key : key(Key.UP), protected : false });

		addBinding(AimDown, { modifiers : [key(Key.CTRL)], key : key(Key.S), protected : false });
		addBinding(AimDown, { modifiers : [key(Key.CTRL)], key : key(Key.DOWN), protected : false });

		addBinding(AimLeft, { modifiers : [key(Key.CTRL)], key : key(Key.A), protected : false });
		addBinding(AimLeft, { modifiers : [key(Key.CTRL)], key : key(Key.Q), protected : false });
		addBinding(AimLeft, { modifiers : [key(Key.CTRL)], key : key(Key.LEFT), protected : false });

		addBinding(AimRight, { modifiers : [key(Key.CTRL)], key : key(Key.D), protected : false });
		addBinding(AimRight, { modifiers : [key(Key.CTRL)], key : key(Key.RIGHT), protected : false });

		addBinding(SelectAim, { modifiers : [key(Key.CTRL)], key : key(Key.ENTER), protected : false });
	}

	static public function addBinding(a : Action, b : KeyBinding) {
		var binding = bindings.get(a);

		if (binding == null)	
			binding = new Array();

		var isPresent = false;

		var isEqual = function (cb : KeyBinding) {
			var modifiers = function () return cb.modifiers.filter(function (k : Input) return b.modifiers.contains(k));
			return cb.key.code == b.key.code && cb.key.type == b.key.type && modifiers().length == b.modifiers.length;
		}

		for (ib in binding) {
			if (isEqual(ib)) {
				isPresent = true;
				break;
			}
		}

		if (isPresent)
			return;

		binding.push(b);
		bindings.set(a, binding);

		var keyConflicts : Array<KeyBinding> = candidates.get(b.key.code);

		if (keyConflicts == null) {
			keyConflicts = new Array();
			candidates.set(b.key.code, keyConflicts);
		}

		keyConflicts.push(b);

		var isConflict = function (b1 : KeyBinding, b2 : KeyBinding) {
			var modifiers = b1.modifiers.filter(function (k : Input) return !b2.modifiers.contains(k));
			return b1 != b2 && modifiers.length == 0;
		}

		var actualConflicts : Array<KeyBinding> = keyConflicts.filter((kb) -> isConflict(b, kb));

		conflicts.set(b, actualConflicts);

		var conflictsOf = keyConflicts.filter((kb -> isConflict(kb, b)));

		for (bi in conflictsOf) {
			var actualConflicts = conflicts.get(bi);
			actualConflicts.push(b);
		}
	}

	static public function removeBinding(a : Action, b : KeyBinding){
		var binding = bindings.get(a);

		if (binding == null)	
			return;

		var isEqual = function (cb : KeyBinding) {
			var modifiers = function () return cb.modifiers.filter(function (k : Input) return b.modifiers.contains(k));
			return !cb.protected && cb.key.code == b.key.code && cb.key.type == b.key.type && modifiers().length == b.modifiers.length;
		}

		var nbinding = binding.filter((b) -> !isEqual(b));

		bindings.set(a, nbinding);

		var keyConflicts = candidates.get(b.key.code);
		keyConflicts.remove(b);

		conflicts.remove(b);

		for (cb in keyConflicts) {
			var cs = conflicts.get(cb);

			if(cs != null) {
				cs.remove(b);
			}
		}
	}

	private function probeAux(a : Action, keyPredicate : (Input) -> Bool, modifierPredicate : (Input) -> Bool) {
		var binding = bindings.get(a);
		
		if (binding == null)	
			return false;

		var isActive = function (kb : KeyBinding) {
			var modifiers = function () return kb.modifiers.filter(function (k : Input) return modifierPredicate(k));
			return keyPredicate(kb.key) && modifiers().length == kb.modifiers.length;
		};

		var hasConflict = function (kb : KeyBinding) {
			var conflicts = conflicts.get(kb);

			if (conflicts == null)
				return false;

			var hasConflict = false;

			for (b in conflicts) {
				hasConflict = hasConflict || isActive(b);
			}

			return hasConflict;
		};

		var actives = binding.filter(function (b) { return isActive(b) && !hasConflict(b); });

		var isActive = actives.length > 0;

		if (isActive)
			suspend(Settings.inputSuspend);

		return isActive;
	}

	private function isDown(k : Input) {
		return k.type == dn.legacy.Controller.Mode.Keyboard && ca.isKeyboardDown(k.code)
			|| k.type == dn.legacy.Controller.Mode.Pad && ca.isDown(new dn.legacy.GamePad.PadKey(k.code));
	}

	private function isPressed(k : Input) {
		return k.type == dn.legacy.Controller.Mode.Keyboard && ca.isKeyboardPressed(k.code)
			|| k.type == dn.legacy.Controller.Mode.Pad && ca.isPressed(new dn.legacy.GamePad.PadKey(k.code));
	}

	public function probe(a : Action) {
		return probeAux(a, isPressed, isDown);
	}

	public function cprobe(a : Action) {
		return probeAux(a, isDown, isDown);
	}

	public function suspend(d : Float) {
		Main.ME.controller.suspend(d);
	}

	public static function getKeyName(k : Input) {
		return
			switch (k.type) {
				case dn.legacy.Controller.Mode.Keyboard: hxd.Key.getKeyName(k.code);
				case dn.legacy.Controller.Mode.Pad: "Button " + k.code;
			}
	}

	public static function getActionName(a : Action) {
		return
			switch a {
				case Up : "Up";
				case Down : "Down";
				case Left : "Left";
				case Right : "Right";
				case Select : "Select";
				case Exit : "Exit";
				case Interact : "Interact";
				case Slot1 : "Spell 1";
				case Slot2 : "Spell 2";
				case Slot3 : "Spell 3";
				case Slot4 : "Spell 4";
				case Map : "Map";
				case AimUp : "Aim Up";
				case AimDown : "Aim Down";
				case AimLeft : "Aim Left";
				case AimRight : "Aim Right";
				case SelectAim : "Select aim";
			}
	}
}
