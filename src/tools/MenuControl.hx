/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;
import tools.CustomControl.Action;
import haxe.ds.Option;

interface MenuItem {
	public function over() : Void;
	public function out() : Void;
	public function click() : Void;
	public function logSelect() : Void;
	public function unroll(m : MenuControl) : Void;
	public function rollBack() : Void;
	dynamic public function getName() : String;
}

class MenuControl implements MenuItem {
	var items : Array<MenuItem> = new Array();
	var selected : Int;
	public var control(default, null) : tools.CustomControl;

	var nextAction = Down;
	var backAction = Up;
	var unrollAction = Right;
	var rollBackAction = Left;

	var sub : Option<MenuControl> = None;
	public var parent(default, null) : Option<MenuControl> = None;

	var repl : Hake;

	private var name : String;

	dynamic public function getName() {
		return name;
	}

	public function new(c, repl, name) {
		this.control = c;

		selected = 0;

		this.repl = repl;
		this.name = name;
	}

	public function update() {
		switch (sub) {
			case Some (s):
				s.update();
			case None:
				var oselected = selected;

				if (items.length == 0)
					return;

				if(control.probe(nextAction)) {
					next();
				}

				if(control.probe(backAction)) {
					back();
				}

				if(control.probe(Select)) {
					click();
				}

				if(control.probe(unrollAction)) {
					items[selected].unroll(this);
				}

				if(control.probe(rollBackAction)) {
					items[selected].rollBack();
				}
		}
	}

	public function addItem(i : MenuItem) {
		items.push(i);
	}

	public function removeItem(i : MenuItem) {
		items.remove(i);
	}

	public function clear() {
		items = new Array();
		selected = 0;
	}

	public function next() {
		out();
		selected++;

		if (selected >= items.length)
			selected = 0;

		over();

		onNext(selected);
	}

	dynamic public function onNext(i : Int) {
	}

	public function back() {
		out();
		selected--;

		if (selected < 0)
			selected = items.length - 1;

		over();

		onBack(selected);
	}

	dynamic public function onBack(i : Int) {
	}

	public function select(mi : MenuItem) {
		for (i in 0...items.length) {
			if (items[i] == mi) {
				selected = i;
			} else {
				items[i].out();
			}
		}
	}

	public function click() {
		items[selected].click();
	}

	public function over() {
		items[selected].over();
	}

	public function out() {
		items[selected].out();
	}

	public function unroll(m : MenuControl) {
		parent = Some (m);
		m.sub = Some (this);
	}

	dynamic public function onRollBack() {
	}

	public function rollBack() {
		switch (parent) {
			case Some (m): m.sub = None;
			case None:
		}
		parent = None;

		onRollBack();
	}

	public function logSelect() {
		var options = switch (parent) {
			case Some (_):
				var item = {
					label: "Back",
					select: (function() { rollBack(); }),
				};

				[item];
			case None: [];
		}

		show(options);
	}

	public function show(firstItems : Array<tools.Hake.HakeOption>) {
		var replItems = firstItems;

		for (i in items) {
			var item = {
				label: i.getName(),
				select: i.logSelect,
			};

			replItems.push(item);
		}

		repl.menu(replItems);
	}
}

class VerticalMenuControl extends MenuControl {

	public function new(c, log, name) {
		super(c, log, name);

		nextAction = Down;
		backAction = Up;
		unrollAction = Right;
		rollBackAction = Left;
	}
}

class HorizontalMenuControl extends MenuControl {

	public function new(c, log, name) {
		super(c, log, name);

		nextAction = Right;
		backAction = Left;
		unrollAction = Down;
		rollBackAction = Up;
	}
}
