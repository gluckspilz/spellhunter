/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import dn.Process;

class LightSource {
	public var x(get, set) : Float;
	inline function get_x() return data.x;
	inline function set_x(v : Float) {
		return data.x = v;
   	}
	public var y(get, set) : Float;
	inline function get_y() return data.y;
	inline function set_y(v : Float) {
		return data.y = v;
   	}
	public var radius(get, set) : Float;
	inline function get_radius() return data.z;
	inline function set_radius(v : Float) {
		var ret = data.z = v;
	   	drawLight();
	   	return ret;
	}
	public var wiggles(get, set) : Float;
	inline function get_wiggles() return data.w;
	inline function set_wiggles(v : Float) return data.w = v;
	public var data(default, null) : h3d.Vector;
	var color : h3d.Vector;

	public static var scene(default, null) : h2d.Scene;
	public var light : h2d.Graphics;
	public var shader : vfx.shdr.Light;
	public var visible(get, set) : Bool;
	inline function get_visible() return light.visible;
	inline function set_visible(v : Bool) return light.visible = v;

	public function new(d : h3d.Vector, c : Int) {
		color = h3d.Vector.fromColor(c);
		data = d;

		radius = d.z;

		if (scene == null) {
			scene = new h2d.Scene();
			new tools.RenderContext.BaseRenderContext(scene);
		}

		drawLight();
	}

	public function drawLight() {
		var visible = true;
		if (light != null) {
			light.remove();
			visible = light.visible;
		}

		light = new h2d.Graphics(scene);
		light.beginFill(0xffffff, 0.);
		light.drawCircle(0, 0, 4 * Const.GRID);
		light.endFill();
		light.blendMode = Add;
		light.visible = visible;

		shader = new vfx.shdr.Light();
		light.addShader(shader);
		shader.data = data;
		shader.color = color;
		shader.color.w = 1;

		setPosition();
	}

	public function setColor(c:Int) {
		color.setColor(c);
		color.w = 1;
	}

	public function setPosition() {
		if (light == null)
			return;

		light.x = Game.ME.scroller.x + x;
		light.y = Game.ME.scroller.y + y;
	}

	public function resize() {
		scene.checkResize();

		drawLight();
	}

	public function destroy() {
		light.remove();
	}
}


class Lights extends Process {
	private var sources : Array<LightSource> = new Array();

	var colorTarget(get,never) : h3d.mat.Texture; inline function get_colorTarget() return Boot.ME.ncolorTarget;

	var scene : h2d.Scene;
	var bg : h2d.Bitmap;

	var lightTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);

	public function new() {
		super(Game.ME);

		scene = new h2d.Scene();
		new tools.RenderContext.BaseRenderContext(scene);

		bg = new h2d.Bitmap(h2d.Tile.fromTexture(colorTarget), scene);
		var shader = new vfx.shdr.AddColor(colorTarget, lightTarget);
		bg.addShader(shader);
	}

	public function addLightSource(s : LightSource) {
		sources.push(s);
	}

	public function removeLightSource(s : LightSource) {
		sources.remove(s);
		s.destroy();
	}

	public function render (e:h3d.Engine) {
		engine.pushTarget(lightTarget);
		engine.clear(0, 1);
		if (LightSource.scene != null) {
			LightSource.scene.render(e);
		}
		engine.popTarget();

		scene.render(e);
	}

	override function onResize() {
		super.onResize();

		scene.checkResize();

		lightTarget.resize(engine.width, engine.height);

		for (s in sources) {
			s.resize();
		}

		bg.tile.scaleToSize(engine.width, engine.height);
	}

	override function onDispose() {
		for (s in sources) {
			s.destroy();
		}
	}
}
