/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import dn.Process;
import hxd.Key;

import ui.Components;

@:uiComp("pause-menu")
class PauseMenuContainer extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <pause-menu>
		<button(Title.getTiles()) public id="cont"/>
		<button(Title.getTiles()) public id="restart"/>
		<button(Title.getTiles()) public id="save"/>
		<button(Title.getTiles()) public id="settings"/>
		<button(Title.getTiles()) public id="exit"/>
	</pause-menu>;

	public function new(c : tools.MenuControl, ?parent) {
		super(parent);
		initComponent();

		layout = Vertical;

		c.addItem(this.cont);
		c.addItem(this.restart);
		c.addItem(this.save);
		c.addItem(this.settings);
		c.addItem(this.exit);
	}

}


class PauseMenu extends dn.Process {
	var ca : dn.legacy.Controller.ControllerAccess;
	var control : tools.CustomControl;
	var mask : h2d.Bitmap;

	var center : h2d.Flow;

	var menuControl : tools.MenuControl.VerticalMenuControl;

	var menuContainer : PauseMenuContainer;

	public function new() {
		super(Main.ME);

		createRoot(Boot.ME.uiRoot);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering
		var tf = new h2d.Text(Assets.fontLarge, root);
		tf.text = "PAUSE - press Escape to resume";

		ca = Main.ME.controller.createAccess("modal", true);
		control = new tools.CustomControl(ca);

		Game.ME.pause();

		mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, 1, 1, 0.6), root);
		root.under(mask);

		createChildProcess(
				function(c) {
					// Resize dynamically
					tf.setScale( M.imax(1, Math.floor( w()*0.35 / tf.textWidth )) );
					tf.x = Std.int( w()*0.5 - tf.textWidth*tf.scaleX*0.5 );
					tf.y = Std.int( h()*0.1 - tf.textHeight*tf.scaleY*0.5 );

				}, true
				);

		center = new h2d.Flow(root);
		center.horizontalAlign = center.verticalAlign = Middle;

		menuControl = new tools.MenuControl.VerticalMenuControl(control, Main.ME.repl, "Pause");
		menuContainer = new PauseMenuContainer(menuControl, center);

		menuContainer.cont.label = "Continue";
		menuContainer.restart.label = "Restart";
		menuContainer.save.label = "Save";
		menuContainer.settings.label = "Settings";
		menuContainer.exit.label = "Exit";

		menuContainer.cont.onClick = function() {
			close();
		}
		menuContainer.cont.onOver = function() {
			menuControl.select(menuContainer.cont);
		}

		menuContainer.restart.onClick = function() {
			Game.ME.restartLevel();
			close();
		}
		menuContainer.restart.onOver = function() {
			menuControl.select(menuContainer.restart);
		}

		menuContainer.save.onClick = function() {
			Game.ME.save();
			close();
		}
		menuContainer.save.onOver = function() {
			menuControl.select(menuContainer.save);
		}

		menuContainer.settings.onClick = function() {
			ca.releaseExclusivity();
			new SettingsMenu(this);
		}
		menuContainer.settings.onOver = function() {
			menuControl.select(menuContainer.settings);
		}

		menuContainer.exit.onClick = function() {
			Main.ME.restart();
			close();
		}
		menuContainer.exit.onOver = function() {
			menuControl.select(menuContainer.exit);
		}

		menuControl.logSelect();

		onResize();
		dn.Process.resizeAll();
	}

	override function resume(){
		super.resume();

		ca.takeExclusivity();
		menuControl.logSelect();
	}

	override function onResize() {
		super.onResize();

		mask.scaleX = M.ceil(w());
		mask.scaleY = M.ceil(h());

		var t = Title.getTiles();

		menuContainer.cont.setVerticalMargin(10 * Const.UI_SCALE);
		menuContainer.restart.setVerticalMargin(10 * Const.UI_SCALE);
		menuContainer.save.setVerticalMargin(10 * Const.UI_SCALE);
		menuContainer.settings.setVerticalMargin(10 * Const.UI_SCALE);
		menuContainer.exit.setVerticalMargin(10 * Const.UI_SCALE);

		menuContainer.cont.resize(t.width, t.height);
		menuContainer.restart.resize(t.width, t.height);
		menuContainer.save.resize(t.width, t.height);
		menuContainer.settings.resize(t.width, t.height);
		menuContainer.exit.resize(t.width, t.height);

		center.minWidth = center.maxWidth = w();
		center.minHeight = center.maxHeight = h();
	}

	override function onDispose() {
		super.onDispose();
		ca.dispose();
		Game.ME.resume();
	}

	public function close() {
		if( !destroyed ) {
			destroy();
		}
	}

	override function update() {
		super.update();

		if( control.probe(Exit)) {
			close();
		}

		menuControl.update();
	}
}
