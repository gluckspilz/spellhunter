/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import dn.Process;

class Dispell extends Process {
	var colorTarget(get,never) : h3d.mat.Texture; inline function get_colorTarget() return Boot.ME.colorTarget;
	var altColorTarget(get,never) : h3d.mat.Texture; inline function get_altColorTarget() return Boot.ME.altColorTarget;

	var scene : h2d.Scene;
	var bg1 : h2d.Bitmap;
	var bg2 : h2d.Bitmap;

	public function new() {
		super(Game.ME);

		scene = new h2d.Scene();
		new tools.RenderContext.BaseRenderContext(scene);

		bg1 = new h2d.Bitmap(h2d.Tile.fromTexture(colorTarget));
		bg2 = new h2d.Bitmap(h2d.Tile.fromTexture(altColorTarget));
		bg2.blendMode = Add;
		var shader = new vfx.shdr.Dispell();
		bg2.addShader(shader);
		scene.add(bg1, 1);
		scene.add(bg2, 1);
	}

	public function render (e:h3d.Engine) {
		scene.render(e);
	}

	override function onResize() {
		super.onResize();

		scene.checkResize();

		bg1.tile.scaleToSize(engine.width, engine.height);
		bg2.tile.scaleToSize(engine.width, engine.height);
	}
}
