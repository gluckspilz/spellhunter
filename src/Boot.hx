/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

#if( js )
import pako.Pako;
#end

class Boot extends hxd.App {
	public static var ME : Boot;

	private var objectScene : h2d.Scene;
	public var colorTarget : h3d.mat.Texture;
	public var altColorTarget : h3d.mat.Texture;
	public var maskTarget : h3d.mat.Texture;
	public var dmaskTarget : h3d.mat.Texture;
	public var normalsTarget : h3d.mat.Texture;
	public var ncolorTarget : h3d.mat.Texture;
	public var renderTarget : h3d.mat.Texture;

	private var bg : h2d.Bitmap;

	private var postScene : h2d.Scene;
	private var post : h2d.Bitmap;
	public var postTarget : h3d.mat.Texture;
	public var postRoot : h2d.Object;

	private var uiScene(get, never) : h2d.Scene; inline function get_uiScene() return s2d;
	public var uiRoot : h2d.Object;

	// Boot
	static function main() {
		new Boot();
	}

	// Engine ready
	override function init() {
		ME = this;

#if( js )
		untyped haxe.zip.Compress.run = function(bytes, level = 9) {
			var data = Pako.deflate(new js.lib.Uint8Array(bytes.getData()), {level: level});
			return haxe.io.Bytes.ofData(data.buffer);
		};
#end

		objectScene = new h2d.Scene();
		new tools.RenderContext(objectScene);

		colorTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		ncolorTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		altColorTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		maskTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		dmaskTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		normalsTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		renderTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);

		postScene = new h2d.Scene();
		new tools.RenderContext.BaseRenderContext(postScene);
		bg = new h2d.Bitmap(h2d.Tile.fromTexture(renderTarget));
		postScene.add(bg, 1);

		postRoot = new h2d.Object();
		postScene.add(postRoot, 2);

		postTarget = new h3d.mat.Texture(engine.width, engine.height, [ Target ]);
		post = new h2d.Bitmap(h2d.Tile.fromTexture(postTarget));
		uiScene.add(post, 1);

		uiRoot = new h2d.Object();
		uiScene.add(uiRoot, 2);

		onResize();

		new Main(objectScene, uiScene);
	}

	override function onResize() {
		super.onResize();
		dn.Process.resizeAll();

		post.setScale(Const.SCALE);

		objectScene.checkResize();
		postScene.checkResize();
		uiScene.checkResize();

		colorTarget.resize(engine.width, engine.height);
		altColorTarget.resize(engine.width, engine.height);
		maskTarget.resize(engine.width, engine.height);
		dmaskTarget.resize(engine.width, engine.height);
		normalsTarget.resize(engine.width, engine.height);
		ncolorTarget.resize(engine.width, engine.height);
		renderTarget.resize(engine.width, engine.height);
		postTarget.resize(engine.width, engine.height);

		bg.tile.scaleToSize(engine.width, engine.height);
		post.tile.scaleToSize(engine.width, engine.height);
	}

	override function dispose() {
		super.dispose();

		if (postScene != null)
			postScene.dispose();

		if (uiScene != null)
			uiScene.dispose();
	}

	var speed = 1.0;
	override function update(deltaTime:Float) {
		super.update(deltaTime);

		// Bullet time
		#if debug
		if( hxd.Key.isPressed(hxd.Key.NUMPAD_SUB) || Main.ME.ca.dpadDownPressed() )
			speed = speed>=1 ? 0.33 : 1;
		#end

		var tmod = hxd.Timer.tmod * speed;
		#if debug
		tmod *= hxd.Key.isDown(hxd.Key.NUMPAD_ADD) || Main.ME!=null && Main.ME.ca.ltDown() ? 5 : 1;
		#end
		dn.legacy.Controller.beforeUpdate();
		dn.Process.updateAll(tmod);
	}

	override function render (e:h3d.Engine) {

		engine.pushTargets([colorTarget, normalsTarget, altColorTarget, maskTarget, dmaskTarget]);

		engine.clear(0, 1);
		objectScene.render(e);
		engine.popTarget();

		engine.pushTarget(ncolorTarget);
		engine.clear(0, 1);
		if (Game.ME != null)
			Game.ME.dispells.render(e);
		engine.popTarget();

		engine.pushTarget(renderTarget);
		engine.clear(0, 1);
		if (Game.ME != null)
			Game.ME.lights.render(e);
		engine.popTarget();

		engine.pushTarget(postTarget);

		engine.clear(0, 1);
		postScene.render(e);

		engine.popTarget();

		uiScene.render(e);
	}
}

