/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx;

class Link extends Effect {
    public static var TARGETS : Map<Entity, Int> = new Map();
	var target: Entity;

	override public function new(o : h2d.Object, x, y, target : Entity, s : String) {
		super(o, x, y, s);
		this.target = target;

		var cnt = TARGETS.get(target);

		if (cnt == null || cnt == 0) {
			TARGETS.set(target, 1);
			applyEffect();
		} else {
			TARGETS.set(target, cnt+1);
			destroy();
		}
	}

	override private function applyEffect() {
		root.removeChildren();

		var g = new h2d.Graphics(root);
		g.beginFill(0x6a59a8, 1.);
		g.drawCircle(0, 0, Const.GRID/4);
		g.endFill();
	}

	override public function update() {
		super.update();

		var d = M.dist(target.centerX, target.centerY, x, y);
		var dx = (target.centerX - x) / d;
		var dy = (target.centerY - y) / d;


		var speed = 20;
		var v = speed * tmod;
		if (d > v) {
			x += v * dx;
			y += v * dy;
			applyEffect();
		} else
			destroy();

		if (target.destroyed)
			destroy();
	}

	override public function dispose() {
		super.dispose();
		var cnt = TARGETS.get(target);
		TARGETS.set(target, cnt-1);
	}
}
