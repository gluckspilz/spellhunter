/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx;

class Dispell extends Effect {
	var target: Entity;
	var radius: Float;

	var shader: vfx.shdr.DrawDMask;

	override public function new(o : h2d.Object, x, y, s) {
		super(o, x, y, s);

		var cx = Math.floor(x/Const.GRID);
		var cy = Math.floor(y/Const.GRID);

		var lifeSpan = 1;

		cd.setS("life", lifeSpan, destroy);

		radius = Const.GRID;

		var g = new h2d.Graphics(root);
		g.beginFill(0xffffff, 0);
		g.drawCircle(0, 0, radius);
		g.endFill();

		shader = new vfx.shdr.DrawDMask((x + game.scroller.x), (y + game.scroller.y));
		shader.radius = radius;
		g.addShader(shader);
	}
}
