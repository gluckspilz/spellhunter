/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class Shockwave extends hxsl.Shader {
	static var SRC = {
		@:import h3d.shader.Base2d;

		@global var screenSize : Vec2;
		@global var offset : Vec2;
		@global var resolution : Vec2;

		@param var radius : Float;
		@param var center : Vec2;
		@global var bgTexture : Sampler2D;
		@param var thickness : Float;


		function pixelise(x : Vec2) : Vec2 {
			var t0 = offset/screenSize;
			var t1 = (offset+resolution)/screenSize;
			var tdiff = t1 - t0;

			var nx = (1/tdiff)*x-(t0/tdiff);
			nx = floor(nx * resolution) / resolution;
			return (nx + (t0/tdiff)) * tdiff;
		}

        function fragment() {
			var pos = (outputPosition.xy + vec2(1)) * 0.5;

			var c = pos * screenSize - (center + offset);
			var r = sqrt(c.x*c.x + c.y*c.y);

			if (abs(radius - r) < thickness) {
				var angle = atan(c.y, c.x);
				pos = vec2(center.x + offset.x + radius * cos(angle), center.y + offset.y + radius * sin(angle));
				pos /= vec2(screenSize);
			}

			pos = pixelise(pos);

			output.color = bgTexture.get(pos);
			output.color.w = 1;


			if (abs(radius - r) < thickness) {
				output.color *= 1.1;
			}
        }
	}

	public function new(centerX : Float, centerY : Float) {
		super();

		center.x = centerX;
		center.y = centerY;

		thickness = 10;
	}
}
