/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class Silhouette extends hxsl.Shader {
    static var SRC = {

		@:import h3d.shader.Base2d;

		@global var screenSize : Vec2;
		@global var offset : Vec2;

		@param var center : Vec2;
		@param var spriteScale : Float;
		@param var color : Vec4;

		function vertex() {
			var pos = vec2((outputPosition.xy + vec2(1)) * 0.5) * screenSize;
			var rcenter = center + offset;


			var v = pos - rcenter;
			v = spriteScale * v;

			var npos = rcenter + v;
			npos = ((npos/screenSize) * 2) - vec2(1);

			output.position = vec4(npos, outputPosition.zw);
		}

		function fragment() {
			if( pixelColor.a > 0.001 )
				output.color = color;
		}
	};


	public function new(centerX:Float, centerY:Float) {
		super();

		color.setColor(0xaf7f66);
		color.w = 1;

		spriteScale = 1.1;

		center.x = centerX;
		center.y = centerY;
	}
}
