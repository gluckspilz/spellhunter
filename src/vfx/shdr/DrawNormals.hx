/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class DrawNormals extends hxsl.Shader {
    static var SRC = {
		@param var normals : Sampler2D;

		@var var calculatedUV : Vec2;

		var pixelNormal : Vec4;

		function fragment() {
			pixelNormal = normals.get(calculatedUV);
		}
    }


	public function new(nt : h2d.Tile) {
		super();

		normals = nt.getTexture();
		normals.filter = Nearest;
	}
}
