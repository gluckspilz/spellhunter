/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class AddColor extends hxsl.Shader {
    static var SRC = {
        @:import h3d.shader.Base2d;

        @param var bgTexture : Sampler2D;
        @param var lightTexture : Sampler2D;

        function fragment() {
            var coord = calculatedUV;

            var light = lightTexture.get(coord);
            var bg = bgTexture.get(coord);

            output.color = light + bg;
        }
    }


    public function new(t : h3d.mat.Texture, lt : h3d.mat.Texture) {
        super();

        bgTexture = t;
        bgTexture.filter = Nearest;

        lightTexture = lt;
        lightTexture.filter = Nearest;
    }
}
