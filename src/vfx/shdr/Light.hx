/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class Light extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@global var offset : Vec2;
		@global var resolution : Vec2;
		@global var screenSize : Vec2;

		@global var normals : Sampler2D;
		@param var lights : Sampler2D;
		@global var noise : Sampler2D;

		@param var data : Vec4;
        @param var color : Vec4;

		@param var speed : Float;

		function getLightWiggle(lightSource : Vec2, strength : Float) : Vec2 {
			var offset = vec2(time * speed);
			var noise = noise.get(offset + (lightSource.xy / screenSize));

			lightSource.x += strength * (2 * noise.x - 1);
			lightSource.y += strength * (2 * noise.y - 1);

			return lightSource;
		}

		function pixelise(x : Vec2) : Vec2 {
			var t0 = offset/screenSize;
			var t1 = (offset+resolution)/screenSize;
			var tdiff = t1 - t0;

			var nx = (1/tdiff)*x-(t0/tdiff);
			nx = floor(nx * resolution) / resolution;
			return (nx + (t0/tdiff)) * tdiff;
		}

        function fragment() {
			var pos = ((outputPosition.xy + vec2(1)) * 0.5);
			pos = pixelise(pos) * screenSize;
			var coord = ((outputPosition.xy + vec2(1)) * 0.5);

			var strength = data.w;
			var radius = data.z;

			var normal = normals.get(coord);
			var pixelnormal = 2*normal.xy - vec2(1);

			var lightPos = offset + data.xy;
			var toLight = getLightWiggle(lightPos, strength) - pos;

			if (normal.w == 0) {
				output.color = vec4(0);
			} else if (normal.z == 1) {
				var brightness = 0.5 * clamp(1.0 - (length(toLight) / radius), 0.0, 1.0);

				output.color = color * brightness;
			} else {
				var brightness = clamp(dot(normalize(toLight), pixelnormal), 0.0, 1.0);
				brightness *= 1.3 * clamp(1.0 - (length(toLight) / radius), 0.0, 1.0);

				output.color = color * brightness;
			}
        }
    }


	public function new() {
		super();

		speed = 0.1;
	}
}
