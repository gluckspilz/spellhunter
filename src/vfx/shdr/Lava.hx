/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class Lava extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@global var offset : Vec2;
		@global var resolution : Vec2;
		@global var screenSize : Vec2;

        @global var noise : Sampler2D;
        @param var lightColor : Vec4;
        @param var color : Vec4;
        @param var darkColor : Vec4;
		@param var flowSpeed : Float;
		@param var noiseSize : Vec2;

		function toTexture(x : Vec2) : Vec2 {
			var t0 = offset/screenSize;
			var t1 = (offset+resolution)/screenSize;
			var tdiff = t1 - t0;

			return (1/tdiff)*x-(t0/tdiff);
		}

        function fragment() {
			var base = (outputPosition.xy + vec2(1)) * 0.5;
			var offset = floor(noiseSize * flowSpeed)/noiseSize;
			var surface1 = noise.get(toTexture(offset+base));
			var surface2 = noise.get(toTexture(-offset+base));
			var v = (surface1 + surface2).r;

			
			var lava = color;
			if (v >= 1.1) {
				lava = darkColor;
			} else if (v < 0.95) {
				lava = lightColor;
			}

            output.color = lava;
        }
    }

	public function new() {
		super();

		darkColor.setColor(0xcf3a26);
		darkColor.w = 1;

		color.setColor(0xee5b41);
		color.w = 1;

		lightColor.setColor(0xf1965e);
		lightColor.w = 1;

		noiseSize = new h3d.Vector(Assets.noiseTexture.width, Assets.noiseTexture.height, 0);

		flowSpeed = .0275;
	}
}
