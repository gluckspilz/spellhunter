/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class DrawAlt extends hxsl.Shader {
    static var SRC = {
		var altPixelColor : Vec4;

		@var var calculatedUV : Vec2;

		@param var texture : Sampler2D;

		function fragment() {
			altPixelColor = texture.get(calculatedUV);
		}
    }


	public function new(t : h3d.mat.Texture) {
		super();

		texture = t;
		texture.filter = Nearest;
	}
}
