/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;

class Bridge extends Entity implements Triggerable {
	public var isActive : Bool;

	private function updateName() {
		if (isActive) {
			proto.removePrefix("Closed");
			proto.addPrefix("Open");
		} else {
			proto.addPrefix("Closed");
			proto.removePrefix("Open");
		}
	}

	public function new (x, y, isTriggerable, isActive) {
		super(x, y, "Bridge");

		isTileEntity = true;

		if (isTriggerable)
			level.addTriggerable(this);

		this.isActive = isActive;

		spr.set(isActive ? dict.Bridge : dict.BridgeClosed);
		updateName();
	}

	override function dispose() {
		super.dispose();

		level.removeTriggerable(this.cx, this.cy);
	}

	public function toggle() {
		game.camera.shakeS(0.2);
		isActive = !isActive;
		spr.set(isActive ? dict.Bridge : dict.BridgeClosed);
		updateName();
		game.repl.log(getLocatedName());
		downlight();
	}

	public function highlight() {
		highlightBase();
	}
}
