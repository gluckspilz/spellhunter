/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;
import haxe.ds.Option;

class ThornyBush extends Entity {
    public static var ALL : Array<ThornyBush> = [];

	var elementable : ent.comp.Elementable;
	var spellable : ent.comp.Spellable;
	var collidable : ent.comp.Collidable;
	var repulsor : ent.comp.Repulsor;

	public function new (x, y) {
		super(x, y, "Thorny bush");

		xr = 0.5;
		yr = 0.5;

		spr.set(dict.ThornyBush);

		level.addCollision(x, y);

		spellable = new ent.comp.Spellable(this);
		elementable = new ent.comp.Elementable(spellable);
		collidable = new ent.comp.Collidable(this, Solid, false, true);
		repulsor = new ent.comp.Repulsor(collidable, 1);

		systems.Kinetic.ME.addEntity(collidable);
	}

	override function onMove() {
		level.removeCollision(this.cx, this.cy);
		var target = new ldtk.Point(Math.floor(cx+dxTotal*Const.GRID), Math.floor(cy+dyTotal*Const.GRID));
		var a = new ent.Arrow(cx, cy, target, dict.ThornyBushProjectile);
		switch [ elementable.state, a.getElementable() ] {
			case [ _, None ]:
			case [(Ice | MeltingIce), Some (e)]: e.coolDown();
			case [ None, _ ]:
			case [ (Burning | Fire), Some (e)]: e.warmUp();
		}
		fx.brokenBits(this.cx, this.cy, a.dxTotal, a.dyTotal, 1, 0x381d1f);
		this.destroy();
		isDead = true;
	}

	override function postUpdate() {
		super.postUpdate();

		if (M.dist(0, 0, dxTotal, dyTotal) > 0.001) {
			onMove();
		}
		else if (isDead && !cd.has("dead"))
			this.destroy();
		else if(!isDead && elementable.state == Fire && !cd.has("fire")) {
			var makeDead =  function () {
				cd.setS("dead", 0.1);
				game.repl.log(getLocatedName() + "has burned down.");
				isDead = true;
			};
			cd.setS("fire", 0.1, true, makeDead);
		}
	}

	override function dispose() {
		super.dispose();

		systems.Kinetic.ME.removeEntity(collidable);

		level.removeCollision(this.cx, this.cy);

		elementable.destroy();
		spellable.destroy();
		collidable.destroy();
		repulsor.destroy();
	}

	override public function getElementable() {
		return Some (elementable);
	}
}
