/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;

class Door extends Entity implements Triggerable {
	public var isActive : Bool;
	private var direction : Enum_DoorDirection;

	private var isVertical(get,never) : Bool;

	inline function get_isVertical() {
		return switch (direction) {
			case Up | Down : false;
			case Left | Right : true;
		};
	}

	private function updateName() {
		if (isActive) {
			proto.addPrefix("Closed");
			proto.removePrefix("Open");
		} else {
			proto.removePrefix("Closed");
			proto.addPrefix("Open");
		}
	}

	public function new (x, y, isTriggerable, isActive, direction) {
		super(x, y, "Door");

		isTileEntity = true;

		if (isTriggerable)
			level.addTriggerable(this);

		this.isActive = isActive;
		this.direction = direction;

		updateName();

		if (isActive) {
			level.addCollision(cx, cy);
			level.addAntiMagic(cx, cy);
		}


		spr.anim.registerStateAnim("HorizontalDoorClosed", 1, () -> !this.isVertical && this.isActive);
		spr.anim.registerStateAnim("HorizontalDoorOpened", 1, () -> !this.isVertical && !this.isActive);
		spr.anim.registerTransition("HorizontalDoorOpened", "HorizontalDoorClosed", "HorizontalDoor", .3, false);
		spr.anim.registerTransition("HorizontalDoorClosed", "HorizontalDoorOpened", "HorizontalDoor", .3, true);
		spr.anim.registerStateAnim("VerticalDoorClosed", 1, () -> this.isVertical && this.isActive);
		spr.anim.registerStateAnim("VerticalDoorOpened", 1, () -> this.isVertical && !this.isActive);
		spr.anim.registerTransition("VerticalDoorOpened", "VerticalDoorClosed", "VerticalDoor", .3, false);
		spr.anim.registerTransition("VerticalDoorClosed", "VerticalDoorOpened", "VerticalDoor", .3, true);
	}

	override function dispose() {
		super.dispose();

		level.removeTriggerable(this.cx, this.cy);
		level.removeCollision(cx, cy);
		level.removeAntiMagic(cx, cy);
	}

	public function toggle() {
		game.camera.shakeS(0.2);
		isActive = !isActive;

		if (isActive) {
			level.addCollision(cx, cy);
			level.addAntiMagic(cx, cy);
			systems.Kinetic.ME.addSlide(cx, cy, direction);
		}
		else {
			level.removeCollision(cx, cy);
			level.removeAntiMagic(cx, cy);
			systems.Kinetic.ME.removeSlide(cx, cy);
		}


		updateName();
		game.repl.log(getLocatedName());

		downlight();
	}

	public function highlight() {
		highlightBase();
	}
}
