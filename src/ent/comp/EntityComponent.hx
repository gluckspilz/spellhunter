/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent.comp;

class EntityComponent {
    public static var ALL : Array<EntityComponent> = [];
    public static var GC : Array<EntityComponent> = [];

	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;
	public var destroyed(default,null) = false;
	public var level(get,never) : Level; inline function get_level() return Game.ME.level;
	public var tmod(get,never) : Float; inline function get_tmod() return Game.ME.tmod;

	public var cd : dn.Cooldown;

	public var me(default, null) : Entity;

    public var cx(get,never) : Int; inline function get_cx() return me.cx;
    public var cy(get,never) : Int; inline function get_cy() return me.cy;
    public var xr(get,never) : Float; inline function get_xr() return me.xr;
    public var yr(get,never) : Float; inline function get_yr() return me.yr;

	public var centerX(get,never) : Float; inline function get_centerX() return me.centerX;
	public var centerY(get,never) : Float; inline function get_centerY() return me.centerY;

	public function new (me : Entity) {
		this.me = me;
		ALL.push(this);

		cd = new dn.Cooldown(Const.FPS);
	}


    public inline function destroy() {
        if( !destroyed ) {
            destroyed = true;
            GC.push(this);
        }
    }


    public function dispose() {
        ALL.remove(this);

		cd.dispose();
		cd = null;
    }

    public function preUpdate() {
		cd.update(tmod);
	}

	public function update() {
	}

    public function postUpdate() {
	}
}
