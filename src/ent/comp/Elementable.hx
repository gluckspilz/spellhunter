/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent.comp;

import haxe.ds.Option;

enum ElementalState {
	Ice;
	MeltingIce;
	None;
	Burning;
	Fire;
}

typedef WarmingData = {
	var time : Float;
	var interrupt : () -> Void;
	var hot : () -> Void;
}

class Elementable extends EntityComponent {
    public static var ALL : Array<ent.comp.Spellable> = [];

	public var state : ElementalState;
	var display : h2d.Object;
	var psdisplay : h2d.Object;

	var spellable : ent.comp.Spellable;

	var lightSource : Lights.LightSource = null;

	public function new (sme : ent.comp.Spellable) {
		super(sme.me);

		var me = sme.me;

		display = new h2d.Object();
		display.x = Std.int(me.centerX);
		display.y = Std.int(me.centerY);
		game.scroller.add(display, Const.DP_FRONT);

		psdisplay = new h2d.Object(Boot.ME.postRoot);
		psdisplay.x = Std.int(me.centerX);
		psdisplay.y = Std.int(me.centerY);

		state = None;

		spellable = sme;

		ALL.push(sme);
	}

	override function dispose() {
		super.dispose();

		systems.Heat.ME.removeEntity(me);

		if (lightSource != null)
			game.lights.removeLightSource(lightSource);

		ALL.remove(spellable);
		display.remove();
		psdisplay.remove();
	}

    override function update() {
		super.update();

		if (lightSource != null) {
			lightSource.x = me.centerX;
			lightSource.y = me.centerY;
			lightSource.setPosition();
		}

		if (state == Fire) {
			if (!cd.hasSetS("fireFx", 0.2)) {
				fx.fire(tools.Geometry.Shape.Circle(me.centerX, me.centerY, (Const.GRID*0.75 + Const.GRID/10)), (_,_) -> false);
			}
		}

		if (state == Burning && !cd.has("warming")) {
			warmUp();
		}

		if (state == Burning) {
			if (!cd.hasSetS("fireFx", 0.2)) {
				fx.fire(tools.Geometry.Shape.Circle(me.centerX, me.centerY, (Const.GRID*0.5)), (_,_) -> false);
			}
		}

		if (state == MeltingIce && !cd.hasSetS("meltFx", 0.2)){
			fx.melt(me.cx, me.cy);
		}
	}

	override function preUpdate() {
		super.preUpdate();

		if (state == Ice)
			me.cancelVelocities();
	}

	override function postUpdate() {
		super.postUpdate();

		if (display != null) {
			display.x = Std.int(me.centerX);
			display.y = Std.int(me.centerY);
		}

		if (psdisplay != null) {
			psdisplay.x = Std.int(me.centerX);
			psdisplay.y = Std.int(me.centerY);
		}
	}

	public function warmUp() {
		if (!cd.has("warming"))
			switch state {
				case Ice:
					game.repl.log(me.getLocatedName() + " is melting.");
					me.removePrefix("Frozen");
					me.addPrefix("Melting");
					state = MeltingIce;
				case MeltingIce:
					me.removePrefix("Melting");
					game.repl.log(me.getLocatedName() + " isn't frozen anymore.");
					state = None;
					clear();
				case None:
					game.repl.log(me.getLocatedName() + " is starting to burn.");
					me.addPrefix("Igniting");
					cd.setS("warming", 0.5);
					state = Burning;
					showFire(Const.GRID/2);
					Assets.sfx.fire(Assets.getSoundEffectsVolume());
				case Burning:
					cd.unset("warming");
					game.repl.log(me.getLocatedName() + " is on fire.");
					state = Fire;
					systems.Heat.ME.addEntity(me);
					me.removePrefix("Igniting");
					me.addPrefix("Burning");
					showFire(Const.GRID*0.75 + Const.GRID/10);
				case Fire:
			}
	}


	public function warmUpSkipTransition() {
		if (!cd.has("warming"))
			switch state {
				case Ice:
					me.removePrefix("Frozen");
					game.repl.log(me.getLocatedName() + " isn't frozen anymore.");
					state = None;
					clear();
				case MeltingIce:
					me.removePrefix("Melting");
					game.repl.log(me.getLocatedName() + " isn't frozen anymore.");
					state = None;
					clear();
				case None:
					game.repl.log(me.getLocatedName() + " is on fire.");
					state = Fire;
					systems.Heat.ME.addEntity(me);
					me.addPrefix("Burning");
					showFire(Const.GRID*0.75 + Const.GRID/10);
					Assets.sfx.fire(Assets.getSoundEffectsVolume());
				case Burning:
					cd.unset("warming");
					game.repl.log(me.getLocatedName() + " is on fire.");
					state = Fire;
					systems.Heat.ME.addEntity(me);
					me.removePrefix("Igniting");
					me.addPrefix("Burning");
					showFire(Const.GRID*0.75 + Const.GRID/10);
				case Fire:
			}
	}

	private function showFire(radius : Float) {
		clear();
		var color = 0xa85959;
		var size = 2*Const.GRID*Const.SCALE;
		var g = new h2d.Graphics(psdisplay);
		g.beginFill(color, 0.);
		g.drawRect(-size/2, -size/2, size, size);
		g.endFill();

		if (Settings.shaders) {
			var shader = new vfx.shdr.Heat(radius, centerX, centerY);
			g.addShader(shader);

			if (lightSource != null) {
				game.lights.removeLightSource(lightSource);

			}

			var pos = new h3d.Vector(centerX, centerY, radius * 4, Const.GRID/2);
			lightSource = new Lights.LightSource(pos, 0xee5b41);
			game.lights.addLightSource(lightSource);
		}
	}

	private function showIce() {
		clear();
        var spr = new HSprite(Assets.sprites, display);
		spr.setCenterRatio(0.5, 0.5);
		spr.set(Assets.spritesDict.Ice);
	}

	public function coolDown() {
		clear();
		cd.unset("warming");
		switch state {
			case Ice:
				showIce();
			case MeltingIce:
				me.removePrefix("Melting");
				game.repl.log(me.getLocatedName() + " isn't melting anymore.");
				me.addPrefix("Frozen");
				state = Ice;
				showIce();
			case None:
				game.repl.log(me.getLocatedName() + " is frozen.");
				me.addPrefix("Frozen");
				me.cancelVelocities();
				state = Ice;
				showIce();
			case Burning:
				me.removePrefix("Igniting");
				game.repl.log(me.getLocatedName() + " isn't catching fire anymore.");
				state = None;
				game.lights.removeLightSource(lightSource);
				lightSource = null;
			case Fire:
				systems.Heat.ME.removeEntity(me);
				me.removePrefix("Burning");
				game.repl.log(me.getLocatedName() + " isn't on fire anymore.");
				state = None;
				game.lights.removeLightSource(lightSource);
				lightSource = null;
		}
	}

	private function clear() {
		display.removeChildren();
		psdisplay.removeChildren();
	}

	public function getWarmable() : Option<WarmingData> {
		return
			switch state {
				case Ice:
					Some ({
						time : 0.7,
						interrupt : coolDown,
						hot : warmUp
					});
				case MeltingIce: None;
				case None:
					Some ({
						time : 0.,
						interrupt : () -> { return; },
						hot : () -> { return; },
					});
				case Burning: None;
				case Fire: None;
			};
	}
}
