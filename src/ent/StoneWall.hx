/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;
import haxe.ds.Option;

class StoneWall extends Entity {

	var spellable : ent.comp.Spellable;
	var collidable : ent.comp.Collidable;
	var repulsor : ent.comp.Repulsor;

	private var markEffect : h2d.Graphics;

	public function new (x, y) {
		super(x, y, "Stone wall");

		isTileEntity = true;
		isFixed = true;

		level.addCollision(x, y);
		level.setSpellableWall(x, y);

		spellable = new ent.comp.Spellable(this);
		collidable = new ent.comp.Collidable(this, Solid, false, true);
		repulsor = new ent.comp.Repulsor(collidable, 2);

		spr.set(dict.Empty);
	}


	override function update() {
		super.update();
	}

	override function dispose() {
		super.dispose();

		level.removeCollision(this.cx, this.cy);
		spellable.destroy();
		collidable.destroy();
		repulsor.destroy();
	}

	override public function unmark() {
		markEffect.remove();
	}

	override public function mark() {
		if (markEffect == null) {
			markEffect = new h2d.Graphics(spr);
			markEffect.drawTile(-Const.GRID/2, -Const.GRID/2, level.getWallTile(cx, cy));
			markEffect.addShader(markShdr);
		}

		if (Settings.shaders) {
			spr.addChild(markEffect);
		}
	}
}
