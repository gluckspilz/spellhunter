/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;
import haxe.ds.Option;

class Spell extends Entity {
    static var ALL : Array<Spell> = [];

	static final anims = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.spell);

	var caster : spl.Caster;
	public var isActive : Bool;
	public var anchor : Option<ent.comp.Spellable>;
	var touching : Array<ent.comp.Collidable>;

	var effectZone : h2d.Graphics;

	var collidable : ent.comp.Collidable;

	public var motion(default, null) : Option<spl.Lang.MotionKind>;
	public var initSpeedX(default, null) : Float;
	public var initSpeedY(default, null) : Float;
	

	public var shouldDispellOnCollide = false;

	var isCollided = false;
	public var isTriggered = false;
	var isNoAnchor = false;

	var lightSource : Lights.LightSource;

	public function new(x, y, mk : Option<spl.Lang.MotionKind>, c : spl.Caster, isActive, canBeCollided) {
		super(0, 0, "Spell");
		setPosPixel(x, y);

		motion = mk;

		game.repl.log(proto.getName() + " was cast at [" + cx + "," + cy + "]");

		canCollide = false;

		caster = c;
		c.addSpell(this);

		this.isActive = isActive;
		anchor = None;
		isDead = false;


		spr.set(Assets.spell);
		spr.setCenterRatio(0.75, 0.5);
		spr.anim.registerStateAnim(anims.GroundSpell, 1, () -> Math.abs(dxTotal) < 0.3 && Math.abs(dyTotal) < 0.3);
		spr.anim.registerStateAnim(anims.SpellProjectile, 1, () -> Math.abs(dxTotal) >= 0.3 || Math.abs(dyTotal) >= 0.3);
		spr.anim.registerStateAnim(anims.NoSpell, 2, () -> Type.enumEq(motion, None));
		spr.anim.registerStateAnim(anims.Empty, 99, () -> isDead);

		spr.anim.registerStateAnim(anims.GroundCollider, 3, () -> switch (motion) { case Some (Attached (_)) : this.isCollided; default : false; });
		spr.anim.registerStateAnim(anims.ProjectileCollider, 3, () -> switch (motion) { case Some (Projectile) : this.isCollided; default : false; });
		spr.anim.registerStateAnim(anims.GroundTrigger, 3, () -> switch (motion) { case Some (Attached (_)) : this.isTriggered; default : false; });
		spr.anim.registerStateAnim(anims.ProjectileTrigger, 3, () -> switch (motion) { case Some (Projectile) : this.isTriggered; default : false; });


		touching = ent.comp.Collidable.ALL.filter(function (ec) { return ec.checkHit(this, level.hasCollision(cx, cy)); });

		Game.ME.scroller.add(spr, Const.DP_FX_BG);
		switch(mk) {
			case Some (Projectile):
				Game.ME.scroller.add(spr, Const.DP_FX_FRONT);

				var t = game.getTarget();
				var mX = t.x;
				var mY = t.y;
				var hX = x;
				var hY = y;

				var dist = M.dist(mX, mY, hX, hY);
				dx = 0.5 * Settings.speed * (mX-hX)/dist;
				dy = 0.5 * Settings.speed * (mY-hY)/dist;

				var offsetX = dx * Const.SPELL_OFFSET + xr;
				var offsetY = dy * Const.SPELL_OFFSET + yr;

				var icx = Std.int(offsetX/Const.GRID);
				var icy = Std.int(offsetY/Const.GRID);

				cx += icx;
				cy += icy;
				xr = offsetX - icx;
				yr = offsetY - icy;

				frict = 1;
				shouldDispellOnCollide = true;

			case Some (Attached (Ground)):
				effectRadius *= 2;
			case Some (Attached (Entity (Caster))):
				effectRadius *= 2;
				anchor = Some (Game.ME.hero.spellable);
			case Some (Attached (Entity (Marked))):
				effectRadius *= 2;
				var candidates = game.marked;
				candidates.remove(Game.ME.hero.spellable);
				var dh = new dn.DecisionHelper(candidates);
				dh.score( (e)->-distCase(e.me) );
				var a = dh.getBest();
				if (a == null) {
					game.repl.log(getLocatedName() + " does not have any marked entity to attach to.");
					isNoAnchor = true;
					makeDispell();
				}
				else
					anchor = Some (dh.getBest());
			case None:
		}

		if (isActive) {
			collisionEShape = Circle(effectRadius);
		}


		collidable = new ent.comp.Collidable(this, Soft, false, canBeCollided);
		collidable.onCheck = shouldDispell;

		collidable.onCollide = function(c) {
			var collided = c;

			switch (anchor) {
				case Some (e):
					switch (e.me.getCollidable()) {
						case Some (ec): collided.remove(ec);
						case None:
					}
				case None:
			}

			for (ec in touching)
				collided.remove(ec);

			for (s in ALL)
				collided.remove(s.collidable);

			if (collided.length > 0) {
				var dh = new dn.DecisionHelper(collided);
				dh.score( (e)->-distCase(e.me) );
				var a = dh.getBest();

				onCollide(a.me);
				if (shouldDispellOnCollide) {
					if (Settings.discrete) {
						this.cx = a.me.cx;
						this.cy = a.me.cx;
					}
					isCollided = true;
					makeDispell();
				}
			}
		};


		var color = 0x6a59a8;
		effectZone = new h2d.Graphics(spr);
		effectZone.beginFill(color, 0.);
		effectZone.lineStyle(2, color);
		effectZone.drawCircle(0,0,effectRadius);
		effectZone.endFill();
		effectZone.visible = false;

		var pos = new h3d.Vector(centerX, centerY, effectRadius*2, 0);
		lightSource = new Lights.LightSource(pos, 0x79acb9);
		game.lights.addLightSource(lightSource);

		if (level.hasAntiMagic(cx,cy)) {
			var log = proto.getName + " gets dispelled at [" + cx + "," + cy + "]";
			new vfx.Dispell(game.scroller, centerX, centerY, log);
			game.lights.removeLightSource(lightSource);
			effectZone.visible = false;
			lightSource.setColor(0xa85959);
			makeDispell();
		}
		else {
			fx.aspirate(x, y, 0x6a59a8, 1.);

		}

		if (!isDead)
			Assets.sfx.castSpell(Assets.getSoundEffectsVolume());


		ALL.push(this);
		initSpeedX = dxTotal;
		initSpeedY = dyTotal;

		systems.Kinetic.ME.addEntity(collidable);
	}

	dynamic public function onCollide(e: Entity) {
	}

	dynamic public function onSpellUpdate() {
	}

	dynamic public function touch() {
	}

	private function shouldDispell() {
		if (!isDead && (level.hasAntiMagic(cx, cy) || !level.isValid(cx, cy))) {
			var log = "Spell gets dispelled at [" + cx + "," + cy + "]";
			new vfx.Dispell(game.scroller, centerX, centerY, log);
			game.lights.removeLightSource(lightSource);
			effectZone.visible = false;
			lightSource.setColor(0xa85959);
			makeDispell();
		}
	}

	override function preUpdate() {
		super.preUpdate();

		if (isDead) {
			cancelVelocities();
		}

	}

	override function update() {
		super.update();

		lightSource.x = centerX;
		lightSource.y = centerY;
		lightSource.setPosition();

		switch (anchor) {
			case Some (e):
				setPosPixel(e.centerX, e.centerY);
			case None:
		}

		if (!isDead)
			onSpellUpdate();

		touching = touching.filter(function (ec) { return ec.checkHit(this, level.hasCollision(cx, cy)); });
	}

	override function postUpdate() {
		super.postUpdate();

		if (!isDead && (!level.isValid(cx, cy) || level.hasAntiMagic(cx, cy))) {
			var log = "Spell gets dispelled at [" + cx + "," + cy + "]";
			new vfx.Dispell(game.scroller, centerX, centerY, log);
			game.lights.removeLightSource(lightSource);
			effectZone.visible = false;
			lightSource.setColor(0xa85959);
			makeDispell();
		}

		if (isDead && !cd.has("dispell"))
			destroy();

		if (!isDead) {
			var angle = Math.atan2(dyTotal, dxTotal);
			spr.rotation = angle;
		}

		if (isDead && Type.enumEq(motion, None))
			spr.alpha *= Math.pow(0.95, tmod);

		switch anchor {
			case Some (e) if (e.me.isDead):
				isNoAnchor = true;
				makeDispell();
			default:
		}

		effectZone.visible = isActive;
	}

	public function makeDispell() {
		if (isDead)
			return;

		if (!isCollided && !isTriggered)
			Assets.sfx.dispell(Assets.getSoundEffectsVolume());

		isDead = true;
		cd.setS("dispell", 1);
	}

	override function dispose() {
		super.dispose();

		game.lights.removeLightSource(lightSource);

		systems.Kinetic.ME.removeEntity(collidable);

		collidable.destroy();

		caster.removeSpell(this);

		ALL.remove(this);
	}

	public function toggleEffect() {
		isActive = !isActive;
		collidable.canCollide = isActive;

		if (isActive) {
			collisionEShape = Circle(effectRadius);
		} else {
			collisionEShape = Circle(properSize);
		}
	}
}
