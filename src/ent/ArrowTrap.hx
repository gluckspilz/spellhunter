/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;


enum Direction {
	Up;
	Down;
	Left;
	Right;
}

class ArrowTrap extends Entity implements Triggerable {
	public var isActive : Bool;
	private var target : ldtk.Point;
	var period : Float;
	var direction : Direction;

	public function new (x, y, p, target, isTriggerable, isActive) {
		super(x, y, "Arrow trap");

		isTileEntity = true;
		period = p;

		if (isTriggerable)
			level.addTriggerable(this);

		this.isActive = isActive;

		level.addCollision(cx, cy);
		level.addAntiMagic(cx, cy);

		this.target = target;

		proto.getLog = function() {
			return proto.getLocatedName() + " aiming towards [" + target.cx + "," + target.cy + "]";
		}

		var angle = Math.atan2(target.cy - cy, target.cx - cx);
		direction = if (Math.cos(angle) > 0.3)
			Right;
		else if (Math.cos(angle) < -0.3)
			Left;
		else if (Math.sin(angle) > 0.3)
			Down;
		else
			Up;
		spr.set(getSpriteName());
	}

	private function getSpriteName() {
		return
			switch (direction) {
				case Right if (isActive): dict.ArrowTrapOn;
				case Right: dict.ArrowTrapOff;
				case Up if (isActive): dict.ArrowTrapUpOn;
				case Up: dict.ArrowTrapUpOff;
				case Down if (isActive): dict.ArrowTrapDownOn;
				case Down: dict.ArrowTrapDownOff;
				case Left if (isActive): dir=-1; dict.ArrowTrapOn;
				case Left: dir=-1; dict.ArrowTrapOff;
			}
	}


	override function dispose() {
		super.dispose();

		level.removeTriggerable(this.cx, this.cy);
		level.removeAntiMagic(cx, cy);
		level.removeCollision(cx, cy);
	}

	public function toggle() {
		isActive = !isActive;
		spr.set(getSpriteName());
	}

    override function update() {
		super.update();

		if (isActive && !cd.hasSetS("shoot", period)) {
			game.repl.log(getLocatedName() + " throws an arrow towards [" + target.cx + "," + target.cy + "]");
			new Arrow(cx, cy, target, dict.Arrow);
		}
	}

	public function highlight() {
		highlightBase();
	}
}
