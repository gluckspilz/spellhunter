/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;

import spl.Token;

class NewSlot extends Entity implements Bonus {
	var nsuid : Game.NSUid;

	public function new (x, y, uid) {
		super(x, y, "New Slot");
		
		level.addBonus(this);

		isTileEntity = true;

		this.nsuid = uid;

		spr.anim.registerStateAnim("NewSlot", 1, 0.2);
	}

	public function capture(){
		game.repl.log("Picked up " + getLocatedName());
		destroy();
		level.removeBonus(cx, cy);
		var id = 0;
		for (i in 1...game.spellCaster.length) {
			if (game.spellCaster[i].isLocked) {
				id = i;
				break;
			}
		}
		game.spellCaster[id] = new spl.Caster(None);
		game.newSpells.set(nsuid, true);
		game.hud.invalidate();
	}
}
