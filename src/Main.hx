/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

import Data;
import hxd.Key;

class MyController extends dn.legacy.Controller {
	public function suspend(s : Float) {
		suspendTemp(s);
	}
}

class MyGameFocusHelper extends dn.heaps.GameFocusHelper {
	override function suspendGame() {
		if (Settings.sightless)
			return;

		super.suspendGame();


	}

	override public function resumeGame() {
		super.resumeGame();
		Key.initialize();
		Main.ME.controller.suspend(0.5);
	}
}

class Main extends dn.Process {
	public static var ME : Main;
	public var controller : MyController;
	public var ca : dn.legacy.Controller.ControllerAccess;

	public var repl : tools.Hake;

	public var gfh : MyGameFocusHelper;

	public function new(s:h2d.Scene, ui:h2d.Scene) {
		super();
		ME = this;

        createRoot(s);

		// Engine settings
		hxd.Timer.wantedFPS = Const.FPS;
		engine.backgroundColor = 0x000;
        #if( hl && !debug )
        engine.fullScreen = true;
        #end

		// Resources
		#if(hl && debug)
		hxd.Res.initLocal();
        #else
        hxd.Res.initEmbed();
        #end

        // Hot reloading
		#if debug
        hxd.res.Resource.LIVE_UPDATE = true;
        hxd.Res.data.watch(function() {
            delayer.cancelById("cdb");

            delayer.addS("cdb", function() {
            	Data.load( hxd.Res.data.entry.getBytes().toString() );
            	if( Game.ME!=null )
                    Game.ME.onCdbReload();
            }, 0.2);
        });
		#end

		// Assets & data init
		Assets.init();
		Settings.init();
		tools.CustomControl.init();
		new ui.Console(Assets.fontTiny, s);
		Lang.init("en");
		Data.load( hxd.Res.data.entry.getText() );

		repl = new tools.Hake();

		// Game controller
		controller = new MyController(ui);
		ca = controller.createAccess("main");
		controller.bind(AXIS_LEFT_X_NEG, Key.LEFT, Key.Q, Key.A);
		controller.bind(AXIS_LEFT_Y_NEG, Key.DOWN, Key.S);
		controller.bind(AXIS_LEFT_X_POS, Key.RIGHT, Key.D);
		controller.bind(AXIS_LEFT_Y_POS, Key.UP, Key.W, Key.Z);
		controller.bind(X, Key.SPACE, Key.F, Key.E);
		controller.bind(B, Key.ENTER, Key.NUMPAD_ENTER);
		controller.bind(SELECT, Key.R);

		gfh = new MyGameFocusHelper(ui, Settings.uiFont);

		ui.add(gfh.root, 10);
		// Start
		delayer.addF( start, 1 );
	}

	public function start() {
		#if !debug
		Assets.playMusic();
		#end

		#if debug
		startGame();
		#else
		new Title();
		#end

		repl.setVisible();
	}

	public function end() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF( () -> new EndScreen(), 2 );
		}
	}

	public function restart() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF( start, 1 );
		}
	}

	public function startGame() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF(function() {
				new Game();
			}, 1);
		}
		else
			new Game();
	}

	public function loadGame() {
		if( Game.ME!=null ) {
			Game.ME.destroy();
			delayer.addF(function() {
				new Game();
				Game.ME.load();
			}, 1);
		}
		else {
			new Game();
			Game.ME.load();
		}
	}

	override public function onResize() {
		super.onResize();
	}

    override function update() {
	    super.update();
    }
}
