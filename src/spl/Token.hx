/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;
import spl.Lang;
import haxe.ds.Option;
import spl.Selector;


enum Token<T> {
	TEntityId : Token<EntityId>;
	TPhysics : Token<Physics>;
    TAnchor : Token<Anchor>;
    TElement : Token<Element>;
	TEffect : Token<Effect>;
	TKind : Token<Kind>;
	TMotionKind : Token<MotionKind>;
	TSpell : Token<Spell>;
}

enum AnyToken {
	AnyEntityIdToken (v : Token<EntityId>);
	AnyPhysicsToken (v : Token<Physics>);
	AnyAnchorToken (v : Token<Anchor>);
	AnyElementToken (v : Token<Element>);
	AnyEffectToken (v : Token<Effect>);
	AnyKindToken (v : Token<Kind>);
	AnyMotionKindToken (v : Token<MotionKind>);
	AnySpellToken (v : Token<Spell>);
}

typedef TokenData<T> = {
	var label : String;
	var extract : (Array<DropdownSelectorComp>) -> Option<T>;
	var token : Token<T>;
	var subs : Array<AnyToken>;
}

enum AnyTokenData {
	AnyEntityIdData (v : TokenData<EntityId>);
	AnyPhysicsData (v : TokenData<Physics>);
	AnyAnchorData (v : TokenData<Anchor>);
	AnyElementData (v : TokenData<Element>);
	AnyEffectData (v : TokenData<Effect>);
	AnyKindData (v : TokenData<Kind>);
	AnyMotionKindData (v : TokenData<MotionKind>);
	AnySpellData (v : TokenData<Spell>);
	AnyNamedSpell (s : Spell);
}

class Util {
	static public function getLabel(d) {
		switch d {
			case (AnyEntityIdData ({ label : label })
			| AnyPhysicsData ({ label : label })
			| AnyAnchorData ({ label : label })
			| AnyElementData ({ label : label })
			| AnyEffectData ({ label : label })
			| AnyKindData ({ label : label })
			| AnyMotionKindData ({ label : label })
			| AnySpellData ({ label : label })) : return label;
			case AnyNamedSpell (l) : return spl.Named.spell(l);
		}
	}

	static public function getToken(d) {
		switch d {
			case AnyEntityIdData ({ token : token }) : return AnyEntityIdToken (token);
			case AnyPhysicsData ({ token : token }) : return AnyPhysicsToken (token);
			case AnyAnchorData ({ token : token }) : return AnyAnchorToken (token);
			case AnyElementData ({ token : token }) : return AnyElementToken (token);
			case AnyEffectData ({ token : token }) : return AnyEffectToken (token);
			case AnyKindData ({ token : token }) : return AnyKindToken (token);
			case AnyMotionKindData ({ token : token }) : return AnyMotionKindToken (token);
			case AnySpellData ({ token : token }) : return AnySpellToken (token);
			case AnyNamedSpell (_) : return AnySpellToken (TSpell);
		}
	}

	static public function compareAnyToken(at1, at2) {
		return
			switch [at1, at2] {
				case [AnyEntityIdToken (_), AnyEntityIdToken (_)]: true;
				case [AnyPhysicsToken (_), AnyPhysicsToken (_)]: true;
				case [AnyAnchorToken (_), AnyAnchorToken (_)]: true;
				case [AnyElementToken (_), AnyElementToken (_)]: true;
				case [AnyEffectToken (_), AnyEffectToken (_)]: true;
				case [AnyKindToken (_), AnyKindToken (_)]: true;
				case [AnyMotionKindToken (_), AnyMotionKindToken (_)]: true;
				case [AnySpellToken (_), AnySpellToken (_)]: true;
				case ([AnyEntityIdToken (_), _]
				| [AnyPhysicsToken (_), _]
				| [AnyAnchorToken (_), _]
				| [AnyElementToken (_), _]
				| [AnyEffectToken (_), _]
				| [AnyKindToken (_), _]
				| [AnyMotionKindToken (_), _]
				| [AnySpellToken (_), _]): false;
			}
	}

	static public function getColor(t) {
		switch t {
			case AnyEntityIdToken (_) : return 0xb169ba;
			case AnyPhysicsToken (_) : return 0xaf7f66;
			case AnyAnchorToken (_) : return 0x79acb9;
			case AnyElementToken (_) : return 0xaf7f66;
			case AnyEffectToken (_) : return 0xaf7f66;
			case AnyKindToken (_) : return 0x7c9a51;
			case AnyMotionKindToken (_) : return 0x79acb9;
			case AnySpellToken (_) : return 0x6a59a8;
		}
	}
}

class LangToToken {
	static public function entityId(r : EntityId) {
		var extract = function (_) { return Some (r); };
		switch r {
			case Caster: return { label : "caster", extract : extract, token : TEntityId, subs : [] };
			case Marked:  return { label : "marked", extract : extract, token : TEntityId, subs : [] };
		}
	}

	static public function physics(p : Physics) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			{ return Some (p); }
		};
		switch p {
			case Push: return { label : "push", extract : extract, token : TPhysics, subs : [] };
			case Teleport:  return { label : "teleport", extract : extract, token : TPhysics, subs : [] };
		}
	}

	static public function anchor(a : Anchor) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			switch a {
				case Ground: { return Some (a); };
				case Entity (_):
					var val =
						switch [ subs[0].extract(TEntityId) ] {
							case [ Some(ei) ]: Some (Entity (ei));
							default: None;
					}
					return val;
			}
		};
		switch a {
			case Ground: return { label : "ground", extract : extract, token : TAnchor, subs : [] };
			case Entity(_): return { label : "entity", extract : extract, token : TAnchor, subs : [ AnyEntityIdToken (TEntityId) ] };
		}
	}

	static public function element(e : Element) {
		var extract = function (_) { return Some (e); };
		switch e {
			case Fire: return { label : "fire", extract : extract, token : TElement, subs : [] };
			case Ice: return { label : "ice", extract : extract, token : TElement, subs : [] };
		}
	}

	static public function kind(k : Kind) {
		var extract = function (_) { return Some (k); };
		switch k {
			case Collider: return { label : "collider", extract : extract, token : TKind, subs : [] };
			case Trigger: return { label : "trigger", extract : extract, token : TKind, subs : [] };
			case Activer: return { label : "activer", extract : extract, token : TKind, subs : [] };
		}
	}

	static public function motionKind(mk : MotionKind) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			switch mk {
				case Projectile:
					return Some (Projectile);
				case Attached(_):
					var val =
						switch [ subs[0].extract(TAnchor) ] {
							case [ Some(a) ]: Some (Attached(a));
							default: None;
					}
					return val;
			}
		};
		switch mk {
			case Projectile: return { label : "projectile", extract : extract, token : TMotionKind, subs : [] };
			case Attached(_): return { label : "attached", extract : extract, token : TMotionKind, subs : [ AnyAnchorToken (TAnchor) ] };
		}
	}

	static public function effect(e : Effect) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			switch e {
				case Elemental(_):
					var val =
						switch [ subs[0].extract(TElement) ] {
							case [ Some(e) ]: Some (Elemental(e));
							default: None;
					}
					return val;
				case Physical(_):
					var val =
						switch [ subs[0].extract(TPhysics) ] {
							case [Some(p)]: Some (Physical(p));
						default: None;
					}
					return val;
				case Sequence(_):
					var val =
						switch [ subs[0].extract(TEffect), subs[1].extract(TEffect) ] {
							case [Some(e1), Some(e2)]: Some (Sequence(e1, e2));
						default: None;
					}
					return val;
				case Meta(_):
					var val =
						switch [ subs[0].extract(TSpell) ] {
							case [Some(s)]: Some (Meta(s));
						default: None;
					}
					return val;
				case Mark:
					return Some (Mark);
				case Link(_):
					var val =
						switch [ subs[0].extract(TEntityId) ] {
							case [Some(ei)]: Some (Link(ei));
						default: None;
					}
					return val;
			}
		};
		switch e {
			case Elemental(_): return { label : "elemental", extract : extract, token : TEffect, subs : [ AnyElementToken (TElement) ] };
			case Physical(_): return { label : "physical", extract : extract, token : TEffect, subs : [ AnyPhysicsToken (TPhysics) ] };
			case Sequence(_): return { label : "sequence", extract : extract, token : TEffect, subs : [ AnyEffectToken (TEffect), AnyEffectToken (TEffect) ] };
			case Meta(_): return { label : "meta", extract : extract, token : TEffect, subs : [ AnySpellToken (TSpell) ] };
			case Mark: return { label : "mark", extract : extract, token : TEffect, subs : [] };
			case Link(_): return { label : "link", extract : extract, token : TEffect, subs : [ AnyEntityIdToken (TEntityId) ] };
		}
	}

	static public function spell(s : Spell) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			switch s {
				case Spell(k, e, mk):
					var val =
						switch [ subs[0].extract(TKind), subs[1].extract(TEffect), subs[2].extract(TMotionKind) ] {
							case [ Some (k), Some(e), Some(mk) ]: Some (Spell(k, e, mk));
							default: None;
					}
					return val;
			}
		};
		switch s {
			case Spell(_): return { label : "spell", extract : extract, token : TSpell, subs : [ AnyKindToken (TKind), AnyEffectToken (TEffect), AnyMotionKindToken (TMotionKind) ] };
		}
	}
}

class LangToSprite {
	static public function addSpellSprite(sprite : h2d.SpriteBatch, s, ?x = 0., ?y = 0.) {
		switch (s) {
			case Spell (_, e, _): getEffectSprite(sprite, e, x, y);
		}
		var l = LangToToken.spell(s);
		var t = Assets.lang.getTile(l.label);
		var e = new h2d.SpriteBatch.BatchElement(t);
		e.x = x;
		e.y = y;
		sprite.add(e);
		return sprite;
	}

	static public function getEffectSprite(sprite : h2d.SpriteBatch, e, ?x = 0., ?y = 0.) {
		var getEffectTile = function (e) {
			  var l = LangToToken.effect(e);
			  return Assets.lang.getTile(l.label);
		};
		switch (e) {
			case Elemental (e): getElementalSprite(sprite, e, x, y);
			case Physical (p): getPhysicalSprite(sprite, p, x, y);
			case Sequence (_):
			   var t = getEffectTile(e);
			   var e = new h2d.SpriteBatch.BatchElement(t);
			   e.x = x;
			   e.y = y;
			   sprite.add(e);
			case Meta (_):
			   var t = getEffectTile(e);
			   var e = new h2d.SpriteBatch.BatchElement(t);
			   e.x = x;
			   e.y = y;
			   sprite.add(e);
			case Mark:
			   var t = getEffectTile(e);
			   var e = new h2d.SpriteBatch.BatchElement(t);
			   e.x = x;
			   e.y = y;
			   sprite.add(e);
			case Link(_):
			   var t = getEffectTile(e);
			   var e = new h2d.SpriteBatch.BatchElement(t);
			   e.x = x;
			   e.y = y;
			   sprite.add(e);
		}
	}

	static public function getElementalSprite(sprite : h2d.SpriteBatch, e, ?x = 0., ?y = 0.) {
		var l = LangToToken.element(e);
		var t = Assets.lang.getTile(l.label);
		var e = new h2d.SpriteBatch.BatchElement(t);
		e.x = x;
		e.y = y;
		sprite.add(e);
	}

	static public function getPhysicalSprite(sprite : h2d.SpriteBatch, p, ?x = 0., ?y = 0.) {
		var l = LangToToken.physics(p);
		var t = Assets.lang.getTile(l.label);
		var e = new h2d.SpriteBatch.BatchElement(t);
		e.x = x;
		e.y = y;
		sprite.add(e);
	}
}
