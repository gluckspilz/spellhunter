/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;
import ui.Components;
import spl.Lang;
import spl.Token;
import haxe.ds.Option;

#if( js )
import js.Browser.document;
#end

typedef Options = {
	var addDropdown : Option<(h2d.Flow) -> Void>;
	var addToDropdownLayer : Option<(h2d.Object) -> Void>;
	var allChoices : Array<AnyTokenData>;
	var newWords : Map<String, Bool>;
	var repl : tools.Hake;
	var control : tools.CustomControl;
}

class DropdownItem extends h2d.Text {
	public var token : AnyTokenData;

	public function new(t : AnyTokenData, font:h2d.Font, ?parent:h2d.Object) {
		super(font, parent);

		token = t;
	}
}

class WrapperComp extends ButtonComp {
	private function getTiles(i : DropdownItem) {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.lang.tile);
		var hoverTile = new Array();
		var activeTile = new h2d.SpriteBatch(Assets.lang.tile);

		switch (i.token) {
			case AnyNamedSpell (s):
				LangToSprite.addSpellSprite(baseTile, s);
				LangToSprite.addSpellSprite(activeTile, s);
			default:
				var l = i.text;
				baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.lang.getTile(l)));
				hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.lang.getTile(l)));
				activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.lang.getTile(l)));
		}

		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, 32, 32, 0.3)));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, 32, 32, 0.3)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function new(item : DropdownItem, ?parent ) {
		super(getTiles(item), parent);

		setVerticalMargin(5);
		setHorizontalMargin(5);

		initComponent();
	}
}

@:uiComp("dropdown")
class DropdownComp extends h2d.Object implements h2d.domkit.Object implements tools.MenuControl.MenuItem {
	static var SRC = <dropdown>
			<anchor id="bg" />
			<container id="current" />
		</dropdown>;

	var defaultItem : h2d.Text;
	var items : Array<DropdownItem> = new Array();
	var itemsAnchor : h2d.Layers;
	var itemList : h2d.Flow;
	var itemsBackground : h2d.Graphics;

	var isOpen : Bool = false;
	public var selected(default, null) : Option<Int> = None;

	static final activeGrey = 0x777777;
	static final baseGrey = 0x222222;
	static final activeRed : Int = 0xaf7f66;
	static final baseRed : Int = 0xa85959;
	static final activeYellow : Int = 0xefef5e;
	static final baseYellow : Int = 0xfbff8c;

	var activeColor : Int = activeRed;
	var baseColor : Int = baseRed;
	var isActive : Bool = false;

	var baseTile : String;
	var activeTile : String;
	var bgTile : String;
	var baseOffset = 0.;
	var activeOffset = 0.;
	var offsetY : Float;

	var options : Options;

	var menu : tools.MenuControl;
	var parentMenu : tools.MenuControl;

	private function initInteractive(addDropdown, addToDropdownLayer) {
		defaultItem.text = "<select>";

		baseTile = Assets.buttonsDict.SpellButtonUp;
		baseOffset = -6.;
		activeOffset = 0.;

		this.itemsAnchor = new h2d.Layers();
		addToDropdownLayer(this.itemsAnchor);
		this.itemList = new h2d.Flow();
		this.itemsAnchor.add(this.itemList, 1);
		addDropdown(this.itemList);

		this.itemList.layout = Horizontal;
		this.itemList.maxWidth = 5 * (Std.int(32 * Const.UI_SCALE) + 10);
		this.itemList.multiline = true;

		this.current.enableInteractive = true;
		this.current.interactive.shape = new h2d.col.Circle(0, 0, OrbitalContainerComp.circleRadius);

		this.current.interactive.onPush = function(_) {
			bgTile = activeTile;
			offsetY = activeOffset;
			redraw();
		}

		this.current.interactive.onRelease = function(_) {
			bgTile = baseTile;
			offsetY = baseOffset;
			redraw();
		}

		this.current.interactive.onClick = function(_) {
			click();
		};
		this.current.interactive.onOver = function(_) {
			over();
		};
		this.current.interactive.onOut = function(_) {
			out();
		};

		this.itemList.enableInteractive = true;
		this.itemList.interactive.onFocus = function(_) {
			open();
		}
		this.itemList.interactive.onFocusLost = function(_) {
			onFocusLost();
		}

		bgTile = baseTile;
		offsetY = baseOffset;
		updateCurrent();
	}

	private function onFocusLost() {
		this.itemList.removeChildren();
		this.itemsBackground.remove();
		this.itemsBackground = null;
		for (i in items) {
			i.visible = false;
		}
		updateCurrent();
		isOpen = false;
		if (items.length == 1 && this.selected == None) {
			setSelected(Some (0), false);
			onSelect(0);
		}
		menu.rollBack();
	}

	public function new(o : Options, ?parent) {
		super(parent);

		initComponent();

		options = o;

		var txt = new h2d.Text(hxd.res.DefaultFont.get());
		txt.font = Settings.uiFont;
		txt.text = "no spell";
		defaultItem = txt;

		baseTile = Assets.buttonsDict.SpellButtonDown;
		activeTile = Assets.buttonsDict.SpellButtonDown;
		bgTile = baseTile;

		offsetY = baseOffset = activeOffset = 0;

		menu = new tools.MenuControl.HorizontalMenuControl(o.control, o.repl, "");

		updateCurrent();
	}

	public function redraw() {
		var bgColor = baseColor;

		if (isActive)
			bgColor = activeColor;

		drawBg(bgColor);
	}

	function drawBg(color) {
		this.bg.removeChildren();

		var spr = new HSprite(Assets.buttons, bgTile, this.bg);
		spr.setCenterRatio(0.5, 0.5);
		spr.scaleX=Const.UI_SCALE;
		spr.scaleY=Const.UI_SCALE;

		var g = new h2d.Object(this.bg);
		g.y = offsetY;

		var graphics = new h2d.Graphics(g);
		graphics.beginFill(color, 1);
		graphics.drawCircle(0, 0, OrbitalContainerComp.circleRadius-1);
		graphics.endFill();

		switch (selected) {
			case None:
			case Some (idx) if (Reflect.field(Assets.langDict, items[idx].text) == null):
			case Some (idx):
				var spr = new HSprite(Assets.lang, items[idx].text, g);
				spr.setCenterRatio(0.5, 0.5);
				spr.scaleX=Const.UI_SCALE;
				spr.scaleY=Const.UI_SCALE;
		}
	}

	dynamic public function onSelect(i : Int) {
	}

	public function addItem(o : DropdownItem) {
		items.push(o);

		if (items.length == 2) {

			switch ([options.addDropdown, options.addToDropdownLayer]) {
				case [Some (addDropdown), Some (addToDropdownLayer)]:
					initInteractive(addDropdown, addToDropdownLayer);
				default:
			}
		}
	}

	public function cleanItems() {
		close();
		items = new Array();
		selected = None;
		menu.clear();
	}

	private function close() {
		this.itemList.interactive.blur();
		menu.clear();
	}

	private function wrapItem(i: Int, parent) {
			var wrapper = new WrapperComp(items[i], parent);

			wrapper.onOver = function() {
				items[i].visible = true;
			}

			wrapper.onOut = function() {
				items[i].visible = false;
			}

			wrapper.onPush = function() {
				setSelected(Some (i), false);
				close();
				onSelect(i);
				onBack();
			};

			wrapper.onClick = function() {
				wrapper.onPush();
				this.current.interactive.focus();
			};

			wrapper.getName = function() {
				return items[i].text;
			};

			menu.addItem(wrapper);

			return wrapper;
	}

	private function open() {
		for (i in 0...items.length) {

			var wrapper = wrapItem(i, this.itemList);
			var colNb = Math.floor(this.itemList.maxWidth / wrapper.width);
			var colNb = 5;
			var row = Math.floor(i / colNb);
			var col = i - row * colNb;

			var width = wrapper.width + wrapper.marginLeft + wrapper.marginRight;
			var height = wrapper.height + wrapper.marginUp + wrapper.marginDown;
			var b = items[i].getBounds();
			var textWidth = b.width;
			var textHeight = b.height;
			items[i].visible = false;
			items[i].setPosition(col * width + width/2 - textWidth/2, row * height + height/2 - textHeight/2);

			this.itemsAnchor.add(items[i], 2);
		}

		var p = this.parent.localToGlobal();
		var b = this.itemList.getBounds();
		this.itemsAnchor.x = p.x - b.width/2;
		this.itemsAnchor.y = p.y - b.height/2;

		var bg = new h2d.Graphics();
		bg.beginFill(0xbdb781, 1.);
		bg.drawRoundedRect(0, 0, b.width, b.height, 10);
		bg.endFill();

		itemsBackground = bg;
		this.itemsAnchor.add(bg, 0);
		menu.over();
	}

	public function setSelected(i, isNew) {
		this.selected = i;
		if (isNew) {
			activeColor = activeYellow;
			baseColor = baseYellow;
		}
		else
			switch i {
				case Some (_):
					activeColor = activeGrey;
					baseColor = baseGrey;
				case None:
					activeColor = activeRed;
					baseColor = baseRed;
			}

		updateCurrent();
	}

	private function updateCurrent() {
		this.current.visible = true;
		this.current.removeChildren();
		this.menu.clear();

		switch this.selected {
			case None:
				this.current.addChild(defaultItem);
				getName = function () {
					return defaultItem.text;
				};
			case Some (i):
				this.current.addChild(items[i]);
				items[i].visible = true;
				getName = function () {
					return items[i].text;
				};
		}
		var b = this.current.getBounds();
		this.x = -b.width/2;
		this.y = -b.height/2;
		if (this.current.enableInteractive) {
			this.current.interactive.x = b.width/2;
			this.current.interactive.y = b.height/2;
		}
		this.bg.x = b.width/2;
		this.bg.y = b.height/2;
		redraw();
	}

	public function click() {
		if (!isOpen) {
			this.current.visible = false;
			isOpen = true;
			this.itemList.interactive.focus();
		}
	}

	dynamic public function onOver() {
	}

	public function over() {
		onOver();
		isActive = true;
		redraw();
	}

	public function out() {
		isActive = false;
		redraw();
	}

	public function logSelect() {
		for (i in 0...items.length) {
			var wrapper = wrapItem(i, null);
		}

		var back = {
			label : "Back",
			select : function() {
				close();
				onBack();
			},
		};
		menu.show([back]);
	}

	public function unroll(m : MenuControl) {
		menu.unroll(m);
	}

	public function rollBack() {
		menu.rollBack();
	}

	dynamic public function getName() {
		return "";
	}

	public function setParentMenu(m : tools.MenuControl) {
		parentMenu = m;
	}

	dynamic public function onBack() {
	}
}

typedef SpellSlotOptions = {
	var edit : (SpellSlotComp, Int) -> Void;
	var currentSlotId : Int;
	var setCurrentSlot : (SpellSlotComp) -> Void;
	var close : () -> Void;
	var openBook : () -> Void;
	var control : tools.MenuControl.VerticalMenuControl;
	var repl : tools.Hake;
}

class SpellSlotComp extends ButtonComp {
	var id : Int;

	var isActive : Bool;

	public function new(o : SpellSlotOptions, id : Int, ?parent) {
		super(ui.Hud.SpellSlotUiComp.getTiles(id), parent);

		label = "spell " + (id + 1);

		this.id = id;

		initComponent();

		if (id == o.currentSlotId) {
			setActive();
			o.setCurrentSlot(this);
		}

		onOut = function() {
			if (isActive)
				showActive();
			else
				showBase();
		};


		onClick = function() {
			o.edit(this, id);
			logMenu();
		};
	}

	public function setActive() {
		dom.active = true;
		isActive = true;
		showActive();
		onSetActive();
	}

	dynamic public function onSetActive() {
	}

	public function unsetActive() {
		dom.active = false;
		isActive = false;
		showBase();
	}

	dynamic public function logMenu() {
	}
}

@:uiComp("slot-container")
class SlotContainerComp extends h2d.Flow implements h2d.domkit.Object implements tools.MenuControl.MenuItem {
	static var SRC = <slot-container>
		</slot-container>;

	var menu : tools.MenuControl;
	var options : SpellSlotOptions;

	var slots : Array<SpellSlotComp>;

	public function redraw() {
		this.removeChildren();
		this.menu.clear();
		this.slots = new Array();

		for (i in 0...Game.ME.spellCaster.length)
			if (!Game.ME.spellCaster[i].isLocked) {
				var s = new SpellSlotComp(options, i, this);
				s.onSetActive = function () {
					options.currentSlotId = i;
				};
				s.logMenu = logSelect;
				menu.addItem(s);

				this.slots.push(s);
			}
	}

	public function new(o : SpellSlotOptions, ?parent) {
		super(parent);

		initComponent();
		options = o;

		menu = new tools.MenuControl.HorizontalMenuControl(o.control.control, o.repl, "");

		slots = new Array();

		redraw();

		this.layout = Horizontal;

	}

	public function over() {
		menu.over();
	}

	public function out() {
		menu.out();
	}

	public function click() {
		menu.click();
	}

	public function logSelect() {
		options.repl.log("Spell " + (options.currentSlotId + 1) + " is currently selected.");

		var back = {
			label : "Back",
			select : function() {
				onBack();
			},
		};

		menu.show([back]);
	}

	public function unroll(m : MenuControl) {
		menu.next();
	}

	public function rollBack() {
		menu.back();
	}

	dynamic public function getName() {
		return "Slots";
	}

	dynamic public function onBack() {
	}

	public function resize() {
		var t = ui.Hud.SpellSlotUiComp.getTiles(0);

		for (s in slots) {
			s.resize(t.width, t.height);
		}
	}
}

@:uiComp("dropdown-selector")
class DropdownSelectorComp extends h2d.Object implements h2d.domkit.Object implements tools.MenuControl.MenuItem {

	var tokenType : AnyToken;
	public var currentToken(default, null) : Option<AnyTokenData>;

	public var subs(default, null) : Array<DropdownSelectorComp> = new Array();

	var choices = new Array();

	var level : Int;

	var menu : tools.MenuControl;
	var parentMenu : tools.MenuControl;

	var orbitalElement : ui.OrbitalElement;

	static var SRC = <dropdown-selector>
		<orbital-container(Util.getColor(token), level) id="suffix" />
		<dropdown(o) id="selector"/>
	</dropdown-selector>;

	private var options : Options;

	private function initLabel() {
		var idx = None;
		var isNew = false;

		if (choices.length == 1) {
		  this.currentToken = Some (choices[0]);
		  this.selector.visible = false;
		  orbitalElement.shouldDraw = false;
		}

		for (i in 0...choices.length) {
			var ltxt = Util.getLabel(choices[i]);

			switch this.currentToken {
				case None:
				case Some (cdata):
					var label = Util.getLabel(cdata);
					if (ltxt == label) {
						idx = Some (i);
						isNew = options.newWords.exists(label);
						menu.getName = () -> label;
					}
			}

			var item = new DropdownItem(choices[i], Settings.uiFont);
			item.text = ltxt;

			this.selector.addItem(item);
		}

		this.selector.setSelected(idx, isNew);
	}

	public function new(o : Options, token : AnyToken, level : Int, t : Option<AnyTokenData>, ?subs : Array<DropdownSelectorComp>, ?parent : h2d.Object) {
		super(parent);
		this.currentToken = t;

		this.level = level;
		this.options = o;
		tokenType = token;

		orbitalElement = new ui.OrbitalElement(this);

		choices = o.allChoices.filter(function (td) {
			var t = Util.getToken(td);
			return Util.compareAnyToken(t, token);
		});

		initComponent();

		menu = new tools.MenuControl.VerticalMenuControl(o.control, o.repl, "");
		menu.onRollBack = function () {
			over();
		};

		this.selector.onBack = logSelect;
		this.selector.setParentMenu(menu);

		initLabel();

		updateSubComponents(subs);

		this.selector.onSelect = function(id) {

			switch (choices[id]) {
				case (AnyNamedSpell (s)):
					var s = Selector.getSpellComponent(o, s, level);
					var subs = s.subs;
					currentToken = s.currentToken;
					updateSubComponents(subs);
					this.selector.cleanItems();
					initLabel();
					redraw();
				default:
					currentToken = Some(choices[id]);
					var subs = null;
					updateSubComponents(subs);
			}

			unroll(parentMenu);
		};

		this.selector.onOver = function() {
			if (parentMenu == null)
				return;
			parentMenu.select(this);
		}
	}

	private function updateSubsComponentsFromTokenData<T>(td:TokenData<T>, ?subs : Array<DropdownSelectorComp>) {
		this.menu.clear();
		for (i in 0...td.subs.length) {
			var c;

			if (subs != null) {
				c = subs[i];
			}
			else {
				c = new DropdownSelectorComp(options, td.subs[i], level+1, None);
			}

			c.setParentMenu(this.menu);
			c.onBack = logSelect;

			this.menu.addItem(c);
			this.suffix.addOrbitalChild(c.orbitalElement, Math.atan2(y, x));
			this.subs.push(c);
		}
	}

	private function updateSubComponents(?subs : Array<DropdownSelectorComp>) {
		this.suffix.remove();
		for (s in this.subs.iterator()) {
			s.remove();
		}
		this.subs = new Array();
		this.suffix = new OrbitalContainerComp(Util.getColor(tokenType), level, this);
		switch currentToken {
			case Some (AnyEntityIdData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyPhysicsData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyAnchorData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyElementData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyEffectData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyKindData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyMotionKindData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnySpellData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			default:
		}
	}

	public function redraw() {
		this.suffix.resize();
		updateSubComponents(this.subs.copy());
		this.selector.redraw();

		for (s in this.subs)
			s.redraw();
	}

	public function extract<T>(ty : Token<T>) : Option<T> {
		switch [ currentToken, ty ] {
			case [ Some (AnyEntityIdData({ extract : extract })), TEntityId ]: return extract(subs);
			case [ Some (AnyPhysicsData({ extract : extract })), TPhysics ]: return extract(subs);
			case [ Some (AnyAnchorData({ extract : extract })), TAnchor ]: return extract(subs);
			case [ Some (AnyElementData({ extract : extract })), TElement ]: return extract(subs);
			case [ Some (AnyEffectData({ extract : extract })), TEffect ]: return extract(subs);
			case [ Some (AnyKindData({ extract : extract })), TKind ]: return extract(subs);
			case [ Some (AnyMotionKindData({ extract : extract })), TMotionKind ]: return extract(subs);
			case [ Some (AnySpellData({ extract : extract })), TSpell ]: return extract(subs);
			default: return None;
		};
	}

	dynamic public function getName() {
		return this.selector.getName();
	}

	public function click() {
		this.selector.click();
		this.selector.unroll(parentMenu);
		out();
	}

	public function over() {
		this.selector.over();
	}

	public function out() {
		this.selector.out();
	}

	public function unroll(m : MenuControl) {
		if (subs.length == 0)
			return;

		menu.unroll(m);
		menu.over();
		out();
	}

	public function rollBack() {
		parentMenu.rollBack();
		out();
	}

	private function extractString() {
		var subString =
		if (subs.length > 0) {
		var subStrings = [for (s in subs) s.extractString()];
		 "(" + subStrings.join(", ") + ")";
		} else {
			"";
		}
		return this.selector.getName() + subString;
	}

	public function logSelect() {
		options.repl.log(extractString());

		var back = {
			label : "Back",
			select : onBack,
		};

		var edit = {
			label : "Edit",
			select : function () {
				this.selector.logSelect();
			},
		};

		menu.show([back, edit]);
	}

	public function setParentMenu(m : MenuControl) {
		parentMenu = m;
	}

	dynamic public function onBack() {
	}
}

@:uiComp("dropdown-container")
class DropdownContainerComp extends h2d.Object implements h2d.domkit.Object {
	var spell : DropdownSelectorComp;
	var startPoint : h2d.col.Point = null;
	var offset = new h2d.col.Point(0, 0);

	var interactive : h2d.Interactive;

	var options : SpellSlotOptions;

	var slotContainer : SlotContainerComp;

	private var width : Float = 0;
	private var height : Float = 0;

	static var SRC = <dropdown-container>
			<container id = "slots" />
			<button(sbTiles) id = "spellbook" />
			<button(dTiles) id = "download" />
			<button(eTiles) id = "exit" />
		</dropdown-container>;

	private function getSpellbookTiles() {
		var w = Std.int(32 * 8 * Const.UI_SCALE);
		var h = Std.int(32 * 10 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	private function getExitTiles() {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.AltarNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.AltarSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.AltarSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	private function getDownloadTiles() {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.DownloadNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.DownloadSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.DownloadSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function new(o : SpellSlotOptions, ?parent) {
		super(parent);

		var sbTiles = getSpellbookTiles();
		var eTiles = getExitTiles();
		var dTiles = getDownloadTiles();

		options = o;

		initComponent();

		this.interactive =  new h2d.Interactive(width, height, this);

		slotContainer = new SlotContainerComp(o, this.slots);
		slotContainer.onBack = options.control.logSelect;

		this.interactive.propagateEvents = true;
		this.interactive.onPush = function(e) {
			startPoint = new h2d.col.Point(e.relX, e.relY);
		};

		this.interactive.onMove = function(e) {
			if (startPoint == null)
				return;

			offset.x += e.relX - startPoint.x;
			offset.y += e.relY - startPoint.y;

			startPoint = new h2d.col.Point(e.relX, e.relY);
		};

		this.interactive.onRelease = function(e) {
			if (startPoint == null)
				return;

			offset.x += e.relX - startPoint.x;
			offset.y += e.relY - startPoint.y;

			startPoint = null;
		};

		this.download.onClick = function (){
			var scene = new h2d.Scene();
			var isSuccess = Selector.ME.addSpellTo(scene);
			if (!isSuccess)
				return;
			var render = scene.captureBitmap();
			var pixels = render.tile.getTexture().capturePixels();
			var png = pixels.toPNG();
#if( js )
			var blob = new js.html.Blob([png.getData()]);
			var url = js.html.URL.createObjectURL(blob);
			var download = document.createAnchorElement();
			download.href = url;
			download.download = "spell.png";
			download.click();
			download.remove();
			js.html.URL.revokeObjectURL(url);
#end
		};
		this.download.getName = () -> "Download";
		this.exit.onClick = o.close;
		this.exit.getName = () -> "Exit";
		this.spellbook.onClick = o.openBook;
		this.spellbook.getName = () -> "Spellbook";
	}

	private function getCenterPoint() {
		var b = spell.getBounds();

		return new h2d.col.Point(x + width/2 - b.width/2, y + height/2);
	}

	public function setSpell (root : h2d.Layers, c : DropdownSelectorComp) {
		options.control.clear();

		c.getName = function () {
			return "Spell";
		};

		options.control.addItem(c);
		options.control.addItem(slotContainer);
		options.control.addItem(this.spellbook);
		options.control.addItem(this.download);
		options.control.addItem(this.exit);

		options.control.over();

		c.setParentMenu(options.control);
		c.onBack = options.control.logSelect;

		spell = c;
		offset.x = 0;
		offset.y = 0;
		var p = getCenterPoint();
		c.x = p.x;
		c.y = p.y;

		root.add(c, 4);
	}

	override private function draw(ctxt) {
		super.draw(ctxt);

		var p = getCenterPoint();
		spell.x = p.x + offset.x;
		spell.y = p.y + offset.y;
	}

	public function resize(w, h) {
		width = w;
		height = h;

		this.interactive.width = width;
		this.interactive.height = height;

		var t = ui.Hud.SpellSlotUiComp.getTiles(0);

		this.slots.x = Std.int( w*0.35 );
		this.slots.y = Std.int( h*0.9 );
		this.slots.y = Std.int( h - t.width - h * 0.005 );

		slotContainer.resize();

		var t = getSpellbookTiles();
		this.spellbook.resize(t.width, t.height);

		var t = getExitTiles();
		this.exit.resize(t.width, t.height);

		this.exit.x = Std.int(w * 0.9);
		this.exit.y = Std.int(h *0.1);

		var t = getDownloadTiles();
		this.download.resize(t.width, t.height);

		this.download.x = Std.int(w * 0.9 - t.width - 10);
		this.download.y = Std.int(h *0.1);

		this.spellbook.x = Std.int(w - spellbook.width);
		this.spellbook.y = Std.int(h - 0.5 * spellbook.height);
	}

	public function redrawSlots() {
		slotContainer.redraw();
	}
}

@:uiComp("selector-tab")
class SelectorTabComp extends h2d.Object implements h2d.domkit.Object {
	public var label : String;

	var img : h2d.Bitmap;

	static var SRC = <selector-tab></selector-tab>;

	public function new(label:String, bgRoot:h2d.Object, t:h2d.Tile, ?parent) {
		super(parent);

		initComponent();

		img = new h2d.Bitmap(t, bgRoot);

		this.label = label;

		hide();
	}

	public function hide() {
		this.visible = false;
		img.visible = false;
		onHide();
	}

	dynamic public function onHide() {
	}

	public function show() {
		this.visible = true;
		img.visible = true;
		onShow();
	}

	dynamic public function onShow() {
	}

	public function resize(w, h) {
		onResize(w, h, img);
	}

	dynamic public function onResize(w, h, img) {
	}
}

@:uiComp("tab-container")
class TabContainerComp extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <tab-container>
			<container id="view" />
		</tab-container>;

	var tabs : Array<SelectorTabComp> = new Array();

	public function new(exit : () -> Void, ?parent) {
		super(parent);

		initComponent();
	}

	public function hideAll() {
		for (t in tabs)
			t.hide();
	}

	public function resize(w, h) {
		for (t in tabs)
			t.resize(w, h);
	}

	public function addTab(t : SelectorTabComp) {
		this.view.addChild(t);
		tabs.push(t);
	}
}

typedef SpellbookOptions = {
	var changeSelector : (spl.Lang.Spell) -> Void;
	var root : h2d.Object;
	var getDefault: () -> Option<spl.Lang.Spell>;
	var displaySpell: (SpellbookComp) -> Void;
	var newWords : () -> Map<String, Bool>;
	var close : () -> Void;
	var control : tools.MenuControl.VerticalMenuControl;
	var repl : tools.Hake;
}

@:uiComp("spellbook")
class SpellbookComp extends h2d.Object implements h2d.domkit.Object {

	static var SRC = <spellbook>
			<container id="spells" />
			<button(tiles) id="close" />
		</spellbook>;

	private var width : Float = 0;
	private var height : Float = 0;

	public function center(o : h2d.Object) {
		var p = getCenterPoint();
		o.x = p.x;
		o.y = p.y;
	}

	var selectedSpell : ButtonComp;
	var options : SpellbookOptions;
	var book : () -> Map<spl.Lang.Spell, Bool>;
	var displaySpell : DropdownSelectorComp;

	private function getExitTiles() {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	private function getSpellTiles(baseTile, hoverTile, activeTile) {
		var w = Std.int(32 * 3.5 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function new(sbo : SpellbookOptions, book:() -> Map<spl.Lang.Spell, Bool>, ?parent) {
		super(parent);

		var tiles = getExitTiles();

		initComponent();

		this.spells.layout = Vertical;
		this.spells.horizontalAlign = Middle;
		this.spells.verticalAlign = Top;
		this.spells.overflow = Scroll;

		options = sbo;
		this.book = book;

		this.close.getName = () -> "Back";
		this.close.onClick = sbo.close;
	}

	public function redraw() {
		this.spells.removeChildren();
		options.control.clear();

		options.control.addItem(this.close);

		var allChoices = new Array();
		for (w in Game.ME.spellWords.iterator())
			allChoices.push(w);
		
		var o = { addDropdown : None
			, addToDropdownLayer : None
			, allChoices : allChoices
			, newWords : options.newWords()
			, control  : options.control.control
			, repl : Main.ME.repl
		};

		var displaySpell = function(s) {
			displaySpell = Selector.getSpellComponent(o, s, 0);
			options.root.addChild(displaySpell);
			displaySpell.redraw();
			center(options.root);
		};

		var map = book();

		if (map == null)
			return;

		for (i in map.keyValueIterator()) {
			if (i.value) {
				var s = Named.spell(i.key);

				var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
				var hoverTile = new Array();
				var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);

				for (j in 0...Game.ME.spellCaster.length)
					if (Type.enumEq(Game.ME.spellCaster[j].formula, Some(i.key))) {
						var t =
							if (j == 0)
								Assets.buttonsDict.SpellSelected1;
							else if (j == 1)
								Assets.buttonsDict.SpellSelected2;
							else if (j == 2)
								Assets.buttonsDict.SpellSelected3;
							else if (j == 3)
								Assets.buttonsDict.SpellSelected4;
							else
								Assets.buttonsDict.SpellSelected1;

						baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(t)));
						hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(t)));
						activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(t)));
					}

				baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellBase)));
				hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellActive)));
				activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellActive)));

				var tiles = getSpellTiles(baseTile, hoverTile, activeTile);

				var t = new ButtonComp(tiles, this);

				t.label = s;
				t.propagateEvents = true;

				t.onClick = function() {
					this.selectedSpell = t;
					options.changeSelector(i.key);
					this.redraw();
					options.repl.log(Lang.Print.spell(i.key));
					options.control.logSelect();
				};

				t.onOver = function () {
					options.root.removeChildren();
					displaySpell(i.key);
					if (this.selectedSpell != null && t != this.selectedSpell)
						this.selectedSpell.showBase();
				};

				t.onOut = function () {
					options.root.removeChildren();
					var s = options.getDefault();
					switch (s) {
						case Some(s):
							displaySpell(s);
						case None:
							options.displaySpell(this);
					}

					if (this.selectedSpell != null)
						this.selectedSpell.showHover();
				};

				options.control.addItem(t);

				if (Type.enumEq(options.getDefault(), Some(i.key))) {
					selectedSpell = t;
					t.showHover();
				}

				this.spells.addChild(t);
			}
		}
	}


	private function getCenterPoint() {
		return new h2d.col.Point(this.x + 3*this.width/5, this.y + 2*this.height/5);
	}

	public function resize(x, y, w, h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;

		redraw();

		this.spells.x = 1*width/4;
		this.spells.y = 1*height/5;
		this.spells.maxHeight = Math.floor(height * 0.5);

		this.close.x = width - 1.5 * width/10;
		this.close.y = height/10;

		var t = getExitTiles();
		this.close.resize(t.width, t.height);

		if (displaySpell != null)
			displaySpell.redraw();
	}
}

class Selector extends dn.Process {
	public static var ME : Selector;

	var ca : dn.legacy.Controller.ControllerAccess = null;
	var control : tools.CustomControl = null;
	var mask : h2d.Bitmap;

	var dropdownLayer : h2d.Object = new h2d.Object();
	var dropdowns : Array<h2d.Flow> = new Array();
	var center : h2d.Object;
	var shouldReset : Bool;
	var canEdit : Bool;

	var currentSlotId : Int = 0;
	var currentSlot : SpellSlotComp;
	var spells : Array<DropdownSelectorComp> = new Array();

	var options : Options;

	var spell : DropdownSelectorComp = null;
	var dropdownContainer : DropdownContainerComp = null;
	var altarT : SelectorTabComp = null;
	var altarControl : tools.MenuControl.VerticalMenuControl;

	var spellbookT : SelectorTabComp;
	var spellbook : SpellbookComp;
	var spellbookDisplay : h2d.Object;
	var spellbookSelected : Option<spl.Lang.Spell>;
	var spellbookControl : tools.MenuControl.VerticalMenuControl;

	var tabContainer : TabContainerComp;

	var book : Map<spl.Lang.Spell, Bool>;
	var newWords :  Map<String, Bool>;

	public function new() {
		super(Main.ME);
		ME = this;
		
		createRoot(Boot.ME.uiRoot);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

		var imgRoot = new h2d.Object();
		root.add(imgRoot, 1);

		ca = Main.ME.controller.createAccess("spell-selector", true);
		ca.releaseExclusivity();

		control = new tools.CustomControl(ca);

		spellbookControl = new tools.MenuControl.VerticalMenuControl(control, Main.ME.repl, "Spellbook");
		altarControl = new tools.MenuControl.VerticalMenuControl(control, Main.ME.repl, "Altar");

		mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, 1, 1, 0.6), root);
		root.under(mask);
		root.add(dropdownLayer, 10);

		center = new h2d.Object();
		root.add(center, 5);

		tabContainer = new TabContainerComp(close, center);

		var openSpellbook = function () {
			tabContainer.hideAll();
			spellbookT.show();

			spellbookControl.logSelect();
		}

		var spellSlotOptions = { edit : edit
			, currentSlotId : currentSlotId
				, setCurrentSlot : function (s) { currentSlot = s; }
			, close : close
				, openBook : openSpellbook
				, control : altarControl
				, repl : Main.ME.repl
		};
		altarT = new SelectorTabComp("altar", imgRoot, hxd.Res.altar.toAseprite().toTile());
		dropdownContainer = new DropdownContainerComp(spellSlotOptions, altarT);

		altarT.onHide = function() {
			for (f in dropdowns)
				f.interactive.blur();

			spell.visible = false;
		};

		altarT.onShow = function() {
			spell.visible = true;

			this.onResize();
		};

		altarT.onResize = function(w, h, img) {
			var scale = dn.heaps.Scaler.bestFit_i(480,270);
			img.x = w*0.5 - img.tile.width*scale*0.5;
			img.y = h - img.tile.height*scale*0.5;
			img.scaleX = scale;
			img.scaleY = scale;

			dropdownContainer.resize(w, h);
		}

		tabContainer.addTab(altarT);

		spellbookDisplay = new h2d.Object();
		root.add(spellbookDisplay, 2);


		spellbookT = new SelectorTabComp("spellbook", imgRoot, hxd.Res.spellbook.toAseprite().toTile());
		var getDefault = function() { return spellbookSelected; };

		var spellbookOptions =
			{ changeSelector : function (s) { changeSelector(s); }
			, root : spellbookDisplay
			, getDefault : getDefault
			, displaySpell : function (s) { displaySpell(s); }
			, newWords : () -> this.newWords
			, close : function () { closeSpellBook(); }
			, control : spellbookControl
			, repl : Main.ME.repl
			};

		var getBook = () -> this.book;

		spellbook = new SpellbookComp(spellbookOptions, getBook, spellbookT);
		tabContainer.addTab(spellbookT);

		spellbookT.onResize = function(w, h, img) {
			var scale = dn.heaps.Scaler.bestFit_i(480,270);
			img.x = w*0.5 - img.tile.width*scale*0.5;
			img.y = h*0.5 - img.tile.height*scale*0.5;
			img.scaleX = scale;
			img.scaleY = scale;

			spellbook.resize(img.x, img.y, img.tile.width*scale, img.tile.height*scale);
		}

		spellbookT.onHide = function() {
			this.spellbookDisplay.removeChildren();
			if (dropdownContainer != null)
				dropdownContainer.setSpell(this.root, spell);
			spell.redraw();
			this.spellbookSelected = None;
		};

		spellbookT.onShow = function () {
			save();

			spellbook.redraw();
			spellbookSelected = spell.extract(TSpell);
			displaySpell(spellbook);

			this.onResize();
		}

		dn.Process.resizeAll();

		pause();
	}

	override function resume(){
		super.resume();
		ca.takeExclusivity();
		Boot.ME.uiRoot.addChild(this.root);
	}

	override function onResize() {
		super.onResize();

		tabContainer.resize(w(), h());
		if (spell != null)
			spell.redraw();

		mask.scaleX = M.ceil(w());
		mask.scaleY = M.ceil(h());
	}

	override function onDispose() {
		super.onDispose();

		ca.releaseExclusivity();
	}

	public function close() {
		for (f in dropdowns) {
			f.interactive.blur();
		}

		tabContainer.hideAll();

		save();

		Game.ME.hud.invalidate();
		Game.ME.resume();
		dn.heaps.slib.SpriteLib.DISABLE_ANIM_UPDATES = false;

		ca.releaseExclusivity();

		if (shouldReset) {
			Game.ME.spawn = new ldtk.Point(Game.ME.hero.cx, Game.ME.hero.cy);
			Game.ME.restartLevel();
		}

		pause();
	}

	private dynamic function closeSpellBook() {
	}

	private dynamic function changeSelector(s : spl.Lang.Spell) {
	}

	private dynamic function displaySpell (spellbook : SpellbookComp) {
	}

	public function open(s : Option<spl.Lang.Spell>, book : Map<spl.Lang.Spell, Bool>, shouldReset : Bool, canEdit : Bool, newWords : Map<String, Bool>) {
		dn.heaps.slib.SpriteLib.DISABLE_ANIM_UPDATES = true;
		ca.takeExclusivity();

		this.book = book;
		this.newWords = newWords;

		Game.ME.pause();
		control.suspend(0.25);

		this.shouldReset = shouldReset;
		this.canEdit = canEdit;

		changeSpell(s);

		if (spell!= null) {
			spellbookSelected = spell.extract(TSpell);
			spell.setParentMenu(this.altarControl);
			spell.onBack = this.altarControl.logSelect;
		}

		displaySpell = function (spellbook : SpellbookComp) {
			var nspell =
				if (canEdit) {
					spell.remove();
					spell;
				} else {
					var o = { addDropdown : None
						, addToDropdownLayer : None
						, allChoices : options.allChoices
						, newWords : newWords
						, control : control
						, repl : Main.ME.repl
					};
					getASpellComponent(o, spell.extract(TSpell));
				}
			spellbookDisplay.addChild(nspell);
			nspell.visible = true;
			nspell.x = 0;
			nspell.y = 0;
			spellbook.center(this.spellbookDisplay);
			nspell.redraw();
		};

		if (canEdit) {
			closeSpellBook = function () {
				tabContainer.hideAll();
				altarT.show();
				altarControl.logSelect();

				spell.setParentMenu(this.altarControl);
				spell.onBack = this.altarControl.logSelect;

				dropdownContainer.redrawSlots();
			};

			changeSelector = function(s) {
				spell.remove();
				spell = getSpellComponent(options, s, 0);
				spell.redraw();
				spell.visible = false;
				dropdownContainer.setSpell(root, spell);
				spellbookSelected = Some (s);
				save();
			}

			altarT.show();
			dropdownContainer.setSpell(root, spell);
			altarControl.logSelect();

		} else {
			closeSpellBook = close;
			changeSelector = function (s) {
				spellbookSelected = Some (s);
			};

			tabContainer.hideAll();
			spellbookT.show();
			spellbookControl.logSelect();
		}

		spellbook.redraw();
		dropdownContainer.redrawSlots();


		resume();
	}

	override function pause() {
		super.pause();

		root.remove();
	}

	override function update() {
		super.update();

		spellbook.center(spellbookDisplay);

		if (spellbookT.visible)
			spellbookControl.update();
		else
			altarControl.update();

		if( control.probe(Exit) || altarT != null && control.probe(Interact) ) {
			if(spellbookT.visible) {
				closeSpellBook();
			} else
				close();
		}

	}

	static public function getPhysicsComponent(o : Options, p : spl.Lang.Physics, level) {
		switch p {
			case Push:
				return new DropdownSelectorComp(o, AnyPhysicsToken (TPhysics), level, Some (AnyPhysicsData(LangToToken.physics(p))));
			case Teleport:
				return new DropdownSelectorComp(o, AnyPhysicsToken (TPhysics), level, Some (AnyPhysicsData(LangToToken.physics(p))));
		}
	}

	static public function getEntityIdComponent(o : Options, r : spl.Lang.EntityId, level) {
		return new DropdownSelectorComp(o, AnyEntityIdToken (TEntityId), level, Some (AnyEntityIdData(LangToToken.entityId(r)))); }

	static public function getElementComponent(o : Options, e : spl.Lang.Element, level) {
		return new DropdownSelectorComp(o, AnyElementToken (TElement), level, Some (AnyElementData(LangToToken.element(e))));
	}

	static public function getEffectComponent(o : Options, e : spl.Lang.Effect, level) {
		switch e {
			case Elemental(el):
				var elComponent = getElementComponent(o, el, level+1);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, Some (AnyEffectData(LangToToken.effect(e))), [elComponent]);
			case Physical(p):
				var pComponent = getPhysicsComponent(o, p, level+1);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, Some (AnyEffectData(LangToToken.effect(e))), [pComponent]);
			case Sequence(e1, e2):
				var e1Component = getEffectComponent(o, e1, level+1);
				var e2Component = getEffectComponent(o, e2, level+1);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, Some (AnyEffectData(LangToToken.effect(e))), [e1Component, e2Component]);
			case Meta(s):
				var sComponent = getSpellComponent(o, s, level+1);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, Some (AnyEffectData(LangToToken.effect(e))), [sComponent]);
			case Mark:
				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, Some (AnyEffectData(LangToToken.effect(e))));
			case Link(ei):
				var eiComponent = getEntityIdComponent(o, ei, level+1);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, Some (AnyEffectData(LangToToken.effect(e))), [eiComponent]);
		}
	}

	static public function getAnchorComponent(o : Options, a : spl.Lang.Anchor, level) {
		switch a {
			case Ground:
				return new DropdownSelectorComp(o, AnyAnchorToken (TAnchor), level, Some (AnyAnchorData(LangToToken.anchor(a))));
			case Entity (ei):
				var eiComponent = getEntityIdComponent(o, ei, level+1);

				return new DropdownSelectorComp(o, AnyAnchorToken (TAnchor), level, Some (AnyAnchorData(LangToToken.anchor(a))), [eiComponent]);
		}
	}

	static public function getMotionKindComponent(o : Options, mk : spl.Lang.MotionKind, level) {
		switch mk {
			case Projectile:
				return new DropdownSelectorComp(o, AnyMotionKindToken (TMotionKind), level, Some (AnyMotionKindData(LangToToken.motionKind(mk))), []);
			case Attached(a):
				var aComponent = getAnchorComponent(o, a, level+1);

				return new DropdownSelectorComp(o, AnyMotionKindToken (TMotionKind), level, Some (AnyMotionKindData(LangToToken.motionKind(mk))), [aComponent]);
		}
	}

	static public function getKindComponent(o : Options, k : spl.Lang.Kind, level) {
		return new DropdownSelectorComp(o, AnyKindToken (TKind), level, Some (AnyKindData(LangToToken.kind(k))));
	}

	static public function getSpellComponent(o : Options, s : spl.Lang.Spell, level) : DropdownSelectorComp {
		switch s {
			case Spell(k, e, mk):
				var kComponent = getKindComponent(o, k, level+1);
				var eComponent = getEffectComponent(o, e, level+1);
				var mkComponent = getMotionKindComponent(o, mk, level+1);

				return new DropdownSelectorComp(o, AnySpellToken (TSpell), level, Some (AnySpellData(LangToToken.spell(s))), [kComponent, eComponent, mkComponent], null);
		}
	}

	public function getASpellComponent(o : Options, s : Option<spl.Lang.Spell>, ?spell) {
		var spl =
			switch s {
				case Some (s):
					getSpellComponent(o, s, 0);
				case None:
					if (spell == null)
						new DropdownSelectorComp(o, AnySpellToken (TSpell), 0, None);
					else
						spell;
			};
		spl.redraw();
		return spl;
	}

	private function addToDropdownLayer(o : h2d.Object){
		dropdownLayer.addChild(o);
	}

	private function addDropdown(f : h2d.Flow){
		dropdowns.push(f);
	}

	private function edit(slot, id) {
		save();
		currentSlot.unsetActive();
		currentSlotId = id;
		currentSlot = slot;
		slot.setActive();
		spell.remove();
		spell = getASpellComponent(options, Game.ME.spellCaster[id].formula, spells[id]);
		dropdownContainer.setSpell(root, spell);
		dropdownContainer.redrawSlots();
	}

	private function save() {
		if (!canEdit)
			return;
		spells[currentSlotId] = spell;
		var spell = spell.extract(TSpell);

		switch spell {
			case Some (s) if (!Game.ME.spellBook.exists(s)): Game.ME.spellBook.set(s, false);
			case Some (_):
			case None:
		};

		Game.ME.spellCaster[currentSlotId] = new Caster(spell);
	}

	public function changeSpell(s : Option<spl.Lang.Spell>) {
		var allChoices = new Array();
		for (w in Game.ME.spellWords.iterator())
			allChoices.push(w);

		options = { addDropdown : Some (addDropdown)
			, addToDropdownLayer : Some (addToDropdownLayer)
			, allChoices : allChoices
			, newWords : new Map()
			, control : control
			, repl : Main.ME.repl
		};

		spell = getASpellComponent(options, s, spell);
	}

	public function addSpellTo(scene : h2d.Scene) {
		var spell = spell.extract(TSpell);

		switch spell {
			case Some (s):
				var o = { addDropdown : None
					, addToDropdownLayer : None
						, allChoices : options.allChoices
						, newWords : new Map()
						, control : control
						, repl : Main.ME.repl
				};

				var sComp = getSpellComponent(o, s, 0);
				sComp.x = 0.1 * w();
				sComp.y = 0.5 * h();
				scene.addChild(sComp);
				return true;
			case None: return false;
		};
	}
}
