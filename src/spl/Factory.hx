/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;
import haxe.ds.Option;

class Factory {
	static public function resolveLinks(es : Array<ent.comp.Spellable>) {
		var linked = new Map();
		for (e in es) {
			switch (e.linkedTo) {
				case None: linked.set(e, true);
				case Some (l):
						   if (!linked.exists(l)) {
							   var s = "The effect is transfered from " + e.me.getLocatedName() + " to " + l.me.getLocatedName();
							   new vfx.Link(Game.ME.scroller, e.centerX, e.centerY, l.me, s);
						   }
						   linked.set(l, true);
			}
		}

		var nes = new Array();
		for (e in linked.keys()) {
			nes.push(e);
		}

		return nes;
	}

	static public function getAffected(s:ent.Spell, caster:ent.comp.Spellable, removeCaster:Bool) {
		var affected = ent.comp.Spellable.ALL.filter(function (se) { var e = se.me; return !e.isDead && s.checkHit(e, true);});
		switch s.anchor {
			case Some (e):
				affected.remove(e);
			case None:
		}

		if (removeCaster)
			affected.remove(caster);

		return resolveLinks(affected);
	}


	static public function getAffectedCells(s:systems.Heat.Source) {
		var candidates = [ for (i in -2...3) for (j in -2...3) Level.Coord.Point(s.cx + i, s.cy + j) ];
		var isIncluded = s.checkHitPosition;
		return candidates.filter(isIncluded);
	}

	static public function buildSpell(s : Option<spl.Lang.Spell>) : (Float, Float, spl.Caster) -> Void {
		switch(s) {
			case Some (Spell (Activer, e, d)):
				var effect = getEffect(e);
				var direction = d;

				return
					function(x, y, c) {
						c.stopCast();
						var s = new ent.Spell(x, y, Some (direction), c, true, true);
						s.onSpellUpdate = function() {
							if (s.isActive)
								effect(s, Game.ME.hero.spellable, c, s);
						};
						s.touch = s.toggleEffect;
					}

			case Some (Spell (Trigger, e, d)):
				var effect = getEffect(e);
				var direction = d;

				return
					function(x, y, c) {
						c.stopCast();
						var s = new ent.Spell(x, y, Some (direction), c, false, false);
						s.onSpellUpdate = function() {};
						s.touch = function() {
							effect(s, Game.ME.hero.spellable, c, s);
							s.isActive = true;
							s.isTriggered = true;
							s.makeDispell();
						};
					}

			case Some (Spell (Collider, e, d)):
				var effect = getEffect(e);
				var direction = d;

				return function(x, y, c) {
					var s = new ent.Spell(x, y, Some (direction), c, false, true);
					s.shouldDispellOnCollide = true;
					s.onCollide = function(e) {
						if (Game.ME.level.hasAntiMagic(e.cx, e.cy)) {
							var log = "Spell gets dispelled at [" + s.cx + "," + s.cy + "]";
							new vfx.Dispell(Game.ME.scroller, s.centerX, s.centerY, log);
							s.makeDispell();
						}

						if (s.isDead)
							return;

						s.isActive = true;
						effect(s, Game.ME.hero.spellable, c, e);
					}
				}
			case None:
				return function(x, y, c) {
					var s = new ent.Spell(x, y, None, c, false, false);
					s.makeDispell();
			   	}
		}
	}

	static public function getEffect(e : spl.Lang.Effect) {
		switch (e) {
			case Elemental(e):
				return getElementalEffect(e);
			case Physical(p):
				return getPhysicsEffect(p);
			case Sequence(e1, e2):
				var e1 = getEffect(e1);
				var e2 = getEffect(e2);
				return function(s : ent.Spell, ecaster : ent.comp.Spellable, caster : Caster, ref : Entity) {
					e1(s, ecaster, caster, ref);
					e2(s, ecaster, caster, ref);
				}
			case Meta(s):
				var ns = buildSpell(Some (s));
				return function(s : ent.Spell, ecaster : ent.comp.Spellable, caster : Caster, ref : Entity) {
					ns(ref.centerX, ref.centerY, caster);
				}
			case Mark:
				return getMarkEffect();
			case Link(ei):
				return getLinkEffect(ei);
		}
	}

	static public function getLinkEffect(ei : spl.Lang.EntityId) {
		return function(s : ent.Spell, ecaster : ent.comp.Spellable, _, _) {

			Main.ME.repl.log( s.getLocatedName() + " applies link effect" );

			var lref =
				switch (ei) {
					case Marked: Game.ME.marked;
					case Caster: [ Game.ME.hero.spellable ];
				}

			var es = getAffected(s, ecaster, false);

			for (e1 in lref) {
				for (e2 in es) {
					e1.link(e2);
				}
			}
		}
	}

	static public function getMarkEffect() {
		return function(s : ent.Spell, ecaster : ent.comp.Spellable, _, _) {
			var es = getAffected(s, ecaster, false);

			Main.ME.repl.log( s.getLocatedName() + " applies mark effect" );

			Game.ME.mark(es);
		}
	}

	static public function getElementalEffect(e : spl.Lang.Element) {
		switch (e) {
			case Fire:
				return function(s : ent.Spell, caster : ent.comp.Spellable, _, _) {
					s.fx.fire(s.effectShape, (_,_) -> false);
					Main.ME.repl.log( s.getLocatedName() + " applies fire effect" );

					systems.Heat.ME.addHeatAction(s, s.anchor);
				}
			case Ice:
				return function(s : ent.Spell, caster : ent.comp.Spellable, _, _) {
					s.fx.snow(s.effectShape);
					Main.ME.repl.log( s.getLocatedName() + " applies ice effect" );

					if (!s.cd.hasSetS("sfxIce", 0.5))
						Assets.sfx.ice(Assets.getSoundEffectsVolume());

					var ignore = switch s.motion {
						case Some (Attached (Entity (Caster))): Some (caster);
						default: None;
					};

					systems.Heat.ME.addCoolAction(s, s.anchor);
				}
		}
	}

	static public function getPhysicsEffect(p : spl.Lang.Physics) {
		return function(s : ent.Spell, caster : ent.comp.Spellable, _, _) {
			switch (p) {
				case Push:
					Assets.sfx.push(Assets.getSoundEffectsVolume());
					var recipients = getAffected(s, caster, true);
					for (r in recipients) {
						switch (s.motion) {
							case Some (Attached (_)):
								var mX = r.centerX;
								var mY = r.centerY;
								var hX = s.centerX;
								var hY = s.centerY;

								var dist = M.dist(mX, mY, hX, hY);
								var dx = mX - hX;
								var dy = mY - hY;

								dx = 0.5 * dx / dist;
								dy = 0.5 * dy / dist;

								systems.Kinetic.ME.superBump(r.me, dx, dy);
							case Some (Projectile):
								systems.Kinetic.ME.superBump(r.me, s.initSpeedX, s.initSpeedY);
							case None:
						}
					}

					var log = "Spell pushes objects at [" + s.cx + "," + s.cy + "]";
					new vfx.Shockwave(Boot.ME.postRoot, s.centerX, s.centerY, 0.3, s.effectRadius, log);
				case Teleport:
					Assets.sfx.teleport(Assets.getSoundEffectsVolume());


					var affected = [caster] ;
					affected = resolveLinks(affected);

					for (e in affected) {
						Main.ME.repl.log( e.me.getLocatedName() + " is teleported to [" + s.cx + "," + s.cy + "]" );
						e.me.teleport(s.centerX, s.centerY);
					}
			}
		}
	}
}
