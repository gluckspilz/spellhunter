/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import ent.comp.Spellable as S;
import spl.Lang;

enum Coord { Point (x : Int, y : Int); }
enum Neighbourhood {
	Moore;
	VonNeumann;
}

class Level extends dn.Process {
	public var TILES(default, null) = new Array();
	public var TEMP_TILES(default, null) : Map<Coord, ProtoEntity> = new Map();
	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;

	public var wid : Int;
	public var hei : Int;

	public var level : World_Level;
	var tilesetSource : h2d.Tile;
	var normalsSource : h2d.Tile;
	var dispellSource : h3d.mat.Texture;
	var maskSource : h3d.mat.Texture;

	var invalidated = true;
	var isHeroNextToAltar = false;

	var wallTile : Map<Coord, h2d.Tile> = new Map();
	var walls : Map<Coord, Bool> = new Map();

	public var collisions(default, null) : Map<Coord, Bool> = new Map();

	var holes : Map<Coord, Bool> = new Map();
	public var antiMagic(default, null) : Map<Coord, Bool> = new Map();
	var endZones : Map<Coord, Bool> = new Map();
	var triggers : Map<Coord, ent.Steppable> = new Map();
	var triggerables : Map<Coord, ent.Triggerable> = new Map();
	var bonuses : Map<Coord, ent.Bonus> = new Map();

	var waters : Map<Coord, Bool> = new Map();
	var ices : Map<Coord, Bool> = new Map();

	var lava : Map<Coord, Bool> = new Map();
	var solidLava : Map<Coord, Bool> = new Map();
	var lavaLights : Map<Coord, Lights.LightSource> = new Map();

	var altars : Map<Coord, Bool> = new Map();

	var texts : h2d.Object;
	var fgTexts : h2d.Object;
	var bg : h2d.Object;
	var col : h2d.Object;
	var dyns : h2d.Object;
	var top : h2d.Object;
	var post : h2d.Object;

	public function new(l : World_Level) {
		super(Game.ME);
		createRootInLayers(Game.ME.scroller, Const.DP_BG);

		level = l;

		tilesetSource = hxd.Res.world.tileset.toAseprite().toTile();
		normalsSource = hxd.Res.world.normals.toAseprite().toTile();
		dispellSource = hxd.Res.world.dispell.toAseprite().getTexture();
		maskSource = hxd.Res.world.idmask.toAseprite().getTexture();

		wid = l.l_Entities.cWid;
		hei = l.l_Entities.cHei;

		texts = new h2d.Object();
		game.scroller.add(texts, Const.DP_BG);

		fgTexts = new h2d.Object();
		game.scroller.add(fgTexts, Const.DP_TOP);

		bg = new h2d.Object(root);
		col = new h2d.Object(root);
		dyns = new h2d.Object(root);
		top = new h2d.Object(root);
		post = new h2d.Object(Boot.ME.postRoot);
	}
	
	override function onDispose() {
		super.onDispose();

		texts.remove();
		fgTexts.remove();

		bg.remove();
		col.remove();
		dyns.remove();
		top.remove();
		post.remove();

		for (t in TILES) {
			t.destroy();
		}
		for (t in TEMP_TILES.iterator()) {
			t.destroy();
		}
		TILES = null;
		TEMP_TILES = null;
	}

	private function initWall(cx, cy) {
		var pe = new ProtoEntity("Wall");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addCollision(cx, cy);
		var pe = new ProtoEntity("Anti-magic field");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addAntiMagic(cx, cy);
	}

	private function initEndZone(cx, cy) {
		var pe = new ProtoEntity("Endzone");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addEndZone(cx, cy);
	}

	private function initAltar(cx, cy) {
		var pe = new ProtoEntity("Altar");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addCollision(cx, cy);
		addAntiMagic(cx, cy);
		var pe = new ProtoEntity("Anti-magic field");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addAltar(cx, cy);
	}

	private function initHole(cx, cy) {
		var pe = new ProtoEntity("Hole");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addHole(cx, cy);
	}

	private function initAntiMagic(cx, cy) {
		var pe = new ProtoEntity("Anti-magic field");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addAntiMagic(cx, cy);
	}

	private function createLavaLight(cx : Int, cy : Int, v : Bool) {
		var x = (cx + 0.5) * Const.GRID;
		var y = (cy + 0.5) * Const.GRID;

		var pos = new h3d.Vector(x, y, Const.GRID * 4, Const.GRID/2);
		var s = new Lights.LightSource(pos, 0xee5b41);
		game.lights.addLightSource(s);
		s.visible = v;
		lavaLights.set(Point(cx, cy), s);
	}

	public function initLevel() {
		for (cx in 0...wid) {
			for (cy in 0...hei) {
				switch (level.l_Collisions.getInt(cx, cy)) {
					case 1: initWall(cx, cy);
					case 2: initEndZone(cx, cy);
					case 3: initAltar(cx, cy);
					case 4: initHole(cx, cy);
					case 5:	new ent.WoodWall(cx, cy);
					case 6: new ent.StoneWall(cx, cy);
					default:
				}

				switch (level.l_TemporaryCollisions.getInt(cx, cy)) {
					case 1:
						createLavaLight(cx, cy, true);	
						addLava(cx, cy);
					case 2:
						createLavaLight(cx, cy, false);	
						addSolidLava(cx, cy);
					case 3: addWater(cx, cy);
					case 4: addIce(cx, cy);
				}

				switch (level.l_AntiMagic.getInt(cx, cy)) {
					case 1: initAntiMagic(cx, cy);
					default:
				}


			}
		}

		for (t in level.l_Collisions.autoTiles) {
			var k = Point(Std.int(t.renderX/Const.GRID), Std.int(t.renderY/Const.GRID));
			if (walls.exists(k)) {
				var tile = level.l_Collisions.untypedTileset.getTile(t.tileId);
				wallTile.set(k, tile);
			}
		}

		for(e in level.l_Entities.all_Text) {
			switch (e.f_isAccessible) {
				case (Both | Sightless):
					game.repl.log(e.f_str);
				case Sighted:
			}
		}

		for (tb in level.l_Entities.all_ThornyBush) {
			new ent.ThornyBush(tb.cx, tb.cy);
		}

		for (s in level.l_Entities.all_NewSpell) {
			var nsuid = Game.NSUid.Id (Game.ME.curLevelIdx, s.cx, s.cy);
			if (!Game.ME.newSpells.exists(nsuid)) {
				var sp = switch s.f_name {
					case FireBolt: Spell (Collider, Elemental (Fire), Projectile);
					case FireTrap: Spell (Collider, Elemental (Fire), Attached (Ground));
					case Push: Spell (Collider, Physical (Push), Projectile);
					case Teleport: Spell (Collider, Physical (Teleport), Projectile);
					case BouncingFire: Spell (Collider, Meta (Spell (Collider, Elemental (Fire), Projectile)), Projectile);
					case IceBolt: Spell (Collider, Elemental (Ice), Projectile);
					case TIceTrap: Spell (Trigger, Elemental (Ice), Attached (Ground));
					case IceShield: Spell (Activer, Elemental (Ice), Attached (Entity (Caster)));
					case IcePush: Spell (Collider, Sequence (Elemental (Ice), Physical (Push)), Projectile);
					case MarkedFire: Spell (Activer, Elemental (Fire), Attached (Entity (Marked)));
					case RecallFromAfar: Spell (Collider, Sequence (Mark, Meta (Spell (Trigger, Physical (Teleport), Attached (Entity (Marked))))), Projectile);
					case Link: Spell (Collider, Link (Caster), Projectile);
				};
				new ent.NewSpell(s.cx, s.cy, nsuid, sp);
			}
		}

		for (s in level.l_Entities.all_NewSlot) {
			var nsuid = Game.NSUid.Id (Game.ME.curLevelIdx, s.cx, s.cy);
			if (!Game.ME.newSpells.exists(nsuid)) {
				new ent.NewSlot(s.cx, s.cy, nsuid);
			}
		}

		for (t in level.l_Entities.all_Trigger) {
			new ent.Trigger(t.cx, t.cy, t.f_targets);
		}

		for (s in level.l_Entities.all_Switch) {
			new ent.Switch(s.cx, s.cy, s.f_targets);
		}

		for (r in level.l_Entities.all_Rock) {
			new ent.Rock(r.cx, r.cy);
		}

		for (b in level.l_Entities.all_Bridge) {
			new ent.Bridge(b.cx, b.cy, b.f_isTriggerable, b.f_isActive);
		}

		for (d in level.l_Entities.all_Door) {
			new ent.Door(d.cx, d.cy, d.f_isTriggerable, d.f_isActive, d.f_direction);
		}

		for (at in level.l_Entities.all_ArrowTrap) {
			new ent.ArrowTrap(at.cx, at.cy, at.f_period, at.f_target, at.f_isTriggerable, at.f_isActive);
		}

		var marked = S.ALL.filter(function (e : ent.comp.Spellable) {
			var marked = level.l_Entities.all_Mark.filter( function(m) { return m.cx == e.cx && m.cy == e.cy; });
		   	return marked.length > 0;
		});

		Game.ME.mark(marked);
	}

	public inline function isValidX(cx) return (cx>=0 && cx<wid);
	public inline function isValidY(cy) return (cy>=0 && cy<hei);
	public inline function isValid(cx,cy) return isValidX(cx) && isValidY(cy);
	public inline function coordId(cx,cy) return cx + cy*wid;


	public function render() {
		texts.removeChildren();
		fgTexts.removeChildren();

		for(e in level.l_Entities.all_Text) {
			switch (e.f_isAccessible) {
				case (Both | Sighted):
					var w = new h2d.Object(texts);
					var tf = new h2d.Text(Settings.gameFont, w);
					tf.text = e.f_str;
					tf.x = Std.int( -tf.textWidth*0.5 );
					tf.y = Std.int( -tf.textHeight*0.5 );
					w.x = Std.int( e.cx * Const.GRID + Const.GRID*0.5 );
					w.y = Std.int( e.cy * Const.GRID + Const.GRID*0.5 );
				case Sightless:
			}
		}

		bg.removeChildren();
		col.removeChildren();
		dyns.removeChildren();
		post.removeChildren();

		for (w in waters.keys()) {
			switch w {
				case Point(cx, cy):
					var sprite = new HSprite(Assets.tiles);
					dyns.addChild(sprite);
					sprite.x = cx * Const.GRID;
					sprite.y = cy * Const.GRID;
					sprite.set(Assets.tilesDict.Water);
			}
		}

		for (i in ices.keys()) {
			switch i {
				case Point(cx, cy):
					var sprite = new HSprite(Assets.tiles);
					dyns.addChild(sprite);
					sprite.x = cx * Const.GRID;
					sprite.y = cy * Const.GRID;
					sprite.set(Assets.tilesDict.Ice);
			}
		}

		for (l in lava.keys()) {
			switch l {
				case Point(cx, cy):


					var sprite = new HSprite(Assets.tiles);
					dyns.addChild(sprite);
					sprite.x = cx * Const.GRID;
					sprite.y = cy * Const.GRID;
					sprite.set(Assets.tilesDict.Lava);

					if (Settings.shaders) {
						var shader = new vfx.shdr.Lava();
						sprite.addShader(shader);
					}

					if (Settings.shaders) {
						var color = 0xa85959;
						var radius = Const.GRID*0.75 + Const.GRID/10;
						var size = 2*Const.GRID;

						var h = new h2d.Graphics(post);
						h.beginFill(color, 0.);
						h.drawRect(-size/2, -size/2, size, size);
						h.endFill();

						h.x = (cx + 0.5) * Const.GRID;
						h.y = (cy + 0.5) * Const.GRID;

						var shader = new vfx.shdr.Heat(radius, h.x, h.y);
						h.addShader(shader);
					}
			}
		}

		for (sl in solidLava.keys()) {
			switch sl {
				case Point(cx, cy):
					var sprite = new HSprite(Assets.tiles);
					dyns.addChild(sprite);
					sprite.x = cx * Const.GRID;
					sprite.y = cy * Const.GRID;
					sprite.set(Assets.tilesDict.SolidLava);
			}
		}

		for (a in altars.keys()) {
			switch a {
				case Point(cx, cy) if (Math.abs(cx - game.hero.cx) <= 1 && Math.abs(cy - game.hero.cy) <= 1):
					var sprite = new HSprite(Assets.tiles);
					dyns.addChild(sprite);
					sprite.x = cx * Const.GRID;
					sprite.y = cy * Const.GRID;
					sprite.set(Assets.tilesDict.Altar);

					if (Settings.shaders) {
						var shader = new vfx.shdr.Silhouette(sprite.x + 0.5 * Const.GRID, sprite.y + 0.5 * Const.GRID);
						sprite.addShader(shader);
					}

					var nsprite = new HSprite(Assets.tiles);
					dyns.addChild(nsprite);
					nsprite.x = cx * Const.GRID;
					nsprite.y = cy * Const.GRID;
					nsprite.set(Assets.tilesDict.Altar);

					if (Settings.textHints) {
						var tf = new h2d.Text(Settings.gameFont, fgTexts);
						tf.text = "Press \"E\"";
						tf.x = sprite.x + Std.int( (Const.GRID -tf.textWidth)*0.5 );
						tf.y = sprite.y + Std.int( -tf.textHeight*0.5 );
					}
				default:
			}
		}

		var top_tg = new h2d.TileGroup(tilesetSource, top);
		level.l_Edges.render(top_tg);

		var bot_tg = new h2d.TileGroup(tilesetSource, bg);
		var tg = new h2d.TileGroup(tilesetSource, col);

		if (Settings.shaders) {
			var shader = new vfx.shdr.DrawNormals(normalsSource);
			shader.setPriority(50);

			bot_tg.addShader(shader);
			tg.addShader(shader);

			var shader = new vfx.shdr.DrawAlt(dispellSource);
			shader.setPriority(50);

			bot_tg.addShader(shader);
			tg.addShader(shader);

			var shader = new vfx.shdr.DrawMask(maskSource);
			shader.setPriority(50);

			bot_tg.addShader(shader);
			tg.addShader(shader);
		}

		level.l_InBound.render(bot_tg);
		level.l_Collisions.render(tg);
	}

	public function hasCollision(cx, cy) : Bool {
		return collisions.exists(Point(cx, cy));
	}

	public function hasAltar(cx, cy) : Bool {
		return altars.exists(Point(cx, cy));
	}

	public function hasHole(cx, cy) : Bool {
		return holes.exists(Point(cx, cy));
	}

	public function hasAntiMagic(cx, cy) : Bool {
		return antiMagic.exists(Point(cx, cy));
	}

	public function hasEndZone(cx, cy) : Bool {
		return endZones.exists(Point(cx, cy));
	}

	public function shouldChangeLevel(cx, cy) : Bool {
		return (cx == 0 || cx == (wid - 1) || cy == 0 || cy == (hei - 1)) && !hasCollision(cx, cy);
	}

	public function hasWarmable(cx, cy) : Bool {
		return hasSolidLava(cx, cy) || hasIce(cx, cy);
	}

	override function update() {
		super.update();

		var tmp = checkNeighbours(Level.Neighbourhood.Moore, hasAltar, game.hero.cx, game.hero.cy);
		if ((isHeroNextToAltar || tmp) && !(isHeroNextToAltar && tmp)) {
			game.repl.log("Press \"E\" to interact with the altar.");
			isHeroNextToAltar = tmp;
			invalidated = true;
		}
	}

	override function postUpdate() {
		super.postUpdate();

		if (!cd.hasSetS("fireFx", 0.2)) {
			for (l in lava.keys()) {
				switch l {
					case Point (x, y):
						fx.fire(tools.Geometry.Shape.Circle((x + 0.5) * Const.GRID, (y + 0.5) * Const.GRID, (Const.GRID*0.75 + Const.GRID/10)), (x, y) -> hasLava(Math.floor(x/Const.GRID), Math.floor(y/Const.GRID)));
				}
			}
		}

		if( invalidated ) {
			invalidated = false;
			render();
		}
	}

	public function addEndZone(cx, cy) {
		endZones.set(Point(cx, cy), true);
	}

	public function removeEndZone(cx, cy) {
		endZones.remove(Point(cx, cy));
	}

	public function addHole(cx, cy) {
		holes.set(Point(cx, cy), true);
	}

	public function removeHole(cx, cy) {
		holes.remove(Point(cx, cy));
	}

	public function addAntiMagic(cx, cy) {
		antiMagic.set(Point(cx, cy), true);
	}

	public function removeAntiMagic(cx, cy) {
		antiMagic.remove(Point(cx, cy));
	}

	public function addTrigger(t:ent.Steppable) {
		triggers.set(Point(t.cx, t.cy), t);
	}

	public function removeTrigger(cx, cy) {
		triggers.remove(Point(cx, cy));
	}

	public function getTrigger(cx, cy) {
		return triggers.get(Point(cx, cy));
	}

	public function addTriggerable(e:ent.Triggerable) {
		triggerables.set(Point(e.cx, e.cy), e);
	}

	public function removeTriggerable(cx, cy) {
		triggerables.remove(Point(cx, cy));
	}

	public function getTriggerable(cx, cy) {
		return triggerables.get(Point(cx, cy));
	}

	public function addCollision(cx, cy) {
		collisions.set(Point(cx, cy), true);
	}

	public function removeCollision(cx, cy) {
		collisions.remove(Point(cx, cy));
	}

	public function addAltar(cx, cy) {
		altars.set(Point(cx, cy), true);
	}

	public function removeAltar(cx, cy) {
		altars.remove(Point(cx, cy));
	}

	public function addIce(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Ice");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		ices.set(c, true);
	}

	public function removeIce(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		ices.remove(c);
	}

	public function hasIce(cx, cy) {
		return ices.exists(Point(cx, cy));
	}

	public function addWater(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Water");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		waters.set(c, true);
	}

	public function removeWater(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		waters.remove(c);
	}

	public function hasWater(cx, cy) {
		return waters.exists(Point(cx, cy));
	}

	public function addSolidLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Solid lava");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		solidLava.set(c, true);
	}

	public function removeSolidLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		solidLava.remove(Point(cx, cy));
	}

	public function hasSolidLava(cx, cy) {
		return solidLava.exists(Point(cx, cy));
	}

	public function addLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Lava");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		lava.set(c, true);
		systems.Heat.ME.addTile(pe);
	}

	public function removeLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		lava.remove(Point(cx, cy));
		systems.Heat.ME.removeTile(pe);
	}

	public function hasLava(cx, cy) {
		return lava.exists(Point(cx, cy));
	}

	public function addBonus(s: ent.Bonus) {
		bonuses.set(Point(s.cx, s.cy), s);
	}

	public function removeBonus(cx, cy) {
		bonuses.remove(Point(cx, cy));
	}

	public function getBonus(cx, cy) {
		return bonuses.get(Point(cx, cy));
	}

	public function setSpellableWall(cx, cy) {
		walls.set(Point(cx, cy), true);
	}

	public function getWallTile(cx, cy) {
		return wallTile.get(Point(cx, cy));
	}

	public function warmUp(cx, cy) {
		if (hasIce(cx, cy)) {
			game.repl.log("Ice[" + cx + "," + cy + "] is turned to water.");
			removeIce(cx, cy);
			addWater(cx, cy);

			invalidated = true;
		}
		else if (hasSolidLava(cx, cy)) {
			game.repl.log("Solid Lava[" + cx + "," + cy + "] is now liquid.");
			removeSolidLava(cx, cy);
			addLava(cx, cy);

			var l = lavaLights.get(Point(cx, cy));
			l.visible = true;

			invalidated = true;
		}
	}

	public function coolDown(cx, cy) {
		if (hasWater(cx, cy)) {
			game.repl.log("Water[" + cx + "," + cy + "] is turned to ice.");
			removeWater(cx, cy);
			addIce(cx, cy);

			invalidated = true;
		}
		else if (hasLava(cx, cy)) {
			game.repl.log("Lava[" + cx + "," + cy + "] is now solid.");
			removeLava(cx, cy);
			addSolidLava(cx, cy);

			var l = lavaLights.get(Point(cx, cy));
			l.visible = false;

			invalidated = true;
		}
	}

	public function checkNeighbours(n : Neighbourhood, f : (cx : Int, cy : Int) -> Bool, cx : Int, cy : Int) {
		var res = switch n {
			case Moore: f(cx+1, cy+1) || f(cx-1, cy-1) || f(cx-1, cy+1) || f(cx+1, cy-1);
			case VonNeumann: false;
		};
		return res || f(cx+1, cy) || f(cx-1, cy) || f(cx, cy+1) || f(cx, cy-1);
	}

	override function onResize() {
		super.onResize();

		invalidated = true;
	}
}
