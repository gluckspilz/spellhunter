/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import haxe.ds.Option;

enum Shape {
	Circle (r : Float);
	Segment;
}

class Entity implements systems.Heat.Source {
    public static var ALL : Array<Entity> = [];
    public static var GC : Array<Entity> = [];

	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;
	public var level(get,never) : Level; inline function get_level() return Game.ME.level;
	public var destroyed(default,null) = false;
	public var ftime(get,never) : Float; inline function get_ftime() return game.ftime;
	public var tmod(get,never) : Float; inline function get_tmod() return Game.ME.tmod;
	public var hud(get,never) : ui.Hud; inline function get_hud() return Game.ME.hud;

	public var cd : dn.Cooldown;

	public var uid : Int;
    public var cx(get, set) : Int;
	inline function get_cx() return proto.cx;
	inline function set_cx(v : Int) return proto.cx = v;
    public var cy(get, set) : Int;
	inline function get_cy() return proto.cy;
	inline function set_cy(v : Int) return proto.cy = v;
    public var xr(get, set) : Float;
	inline function get_xr() return proto.xr;
	inline function set_xr(v : Float) return proto.xr = v;
    public var yr(get, set) : Float;
	inline function get_yr() return proto.yr;
	inline function set_yr(v : Float) return proto.yr = v;

    public var dx = 0.;
    public var dy = 0.;
    public var bdx = 0.;
    public var bdy = 0.;
	public var dxTotal(get,never) : Float; inline function get_dxTotal() return dx+bdx;
	public var dyTotal(get,never) : Float; inline function get_dyTotal() return dy+bdy;
	public var speed(get,never) : Float; inline function get_speed() return M.dist(0, 0, dxTotal, dyTotal);
	public var speedSteps(get,never) : Float; inline function get_speedSteps() return speed * Const.GRID * tmod;
	public var frict = 0.1;
	public var bumpFrict = 0.93;
	public var hei : Float = Const.GRID;
	private var properSize = 0.45 * Const.GRID;
	private var collisionEShape(default, null) : Shape;
	private var effectEShape(get, never) : Shape; inline function get_effectEShape() return Circle(effectRadius);
	public var collisionShape(get, never) : tools.Geometry.Shape; inline function get_collisionShape() return createShape(collisionEShape);
	public var effectShape(get, never) : tools.Geometry.Shape; inline function get_effectShape() return createShape(effectEShape);
	public var effectRadius = Const.GRID*0.75;
	public var collisionRadius(get, never) : Float;

	public var dir(default,set) = 1;
	public var sprScaleX = 1.0;
	public var sprScaleY = 1.0;

    public var spr : HSprite;
	public var colorAdd : h3d.Vector;
	var debugLabel : Null<h2d.Text>;

	public var footX(get,never) : Float; inline function get_footX() return (cx+xr)*Const.GRID;
	public var footY(get,never) : Float; inline function get_footY() return (cy+yr)*Const.GRID;
	public var headX(get,never) : Float; inline function get_headX() return footX;
	public var headY(get,never) : Float; inline function get_headY() return footY-hei;
	public var centerX(get,never) : Float; inline function get_centerX() return footX;
	public var centerY(get,never) : Float; inline function get_centerY() return footY;

	public var canCollide(default, null) = true;
	public var isTileEntity(default, null) = false;
	var isFixed = false;
	public var isDead(default, null) = false;

	var dict = Assets.spritesDict;

	public var proto(default, null) : ProtoEntity;

	public var oldCx : Int;
	public var oldCy : Int;

	private var markShdr : vfx.shdr.MarkEntity;

    public function new(x:Int, y:Int, n : String) {
        uid = Const.NEXT_UNIQ;
        ALL.push(this);

		markShdr = new vfx.shdr.MarkEntity();

		collisionEShape = Circle(properSize);

		this.proto = new ProtoEntity(n);

		cd = new dn.Cooldown(Const.FPS);
		cx = x;
		cy = y;

        spr = new HSprite(Assets.sprites);
        Game.ME.scroller.add(spr, Const.DP_MAIN);
		spr.colorAdd = colorAdd = new h3d.Vector();
		spr.setCenterRatio(0.5, 0.5);
    }

	inline function set_dir(v) {
		return dir = v>0 ? 1 : v<0 ? -1 : dir;
	}

	public inline function isAlive() {
		return !destroyed;
	}

	public function kill(by:Null<Entity>) {
		destroy();
	}

	public function setPosCase(x:Int, y:Int) {
		cx = x;
		cy = y;
		xr = 0;
		yr = 0;
	}

	public function setPosPixel(x:Float, y:Float) {
		cx = Std.int(x/Const.GRID);
		cy = Std.int(y/Const.GRID);
		xr = (x-cx*Const.GRID)/Const.GRID;
		yr = (y-cy*Const.GRID)/Const.GRID;
	}

	private function onMove() {
	}

	public function superBump(x:Float,y:Float) {
		var odx = dxTotal;
		var ody = dyTotal;
		dx=0;
		dy=0;
		bdx=x;
		bdy=y;

		if(odx < 0.03 || ody < 0.03)
			onMove();
	}

	public function bump(x:Float,y:Float) {
		bdx+=x;
		bdy+=y;
	}

	public function isStatic() {
		return M.fabs(dxTotal) < 0.0003 && M.fabs(dyTotal) < 0.0003;
	}

	public function cancelVelocities() {
		dx = bdx = 0;
		dy = bdy = 0;
	}

	public function is<T:Entity>(c:Class<T>) return Std.isOfType(this, c);
	public function as<T:Entity>(c:Class<T>) : T return Std.downcast(this, c);

	public inline function rnd(min,max,?sign) return Lib.rnd(min,max,sign);
	public inline function irnd(min,max,?sign) return Lib.irnd(min,max,sign);
	public inline function pretty(v,?p=1) return M.pretty(v,p);

	public inline function dirTo(e:Entity) return e.centerX<centerX ? -1 : 1;
	public inline function dirToAng() return dir==1 ? 0. : M.PI;
	public inline function getMoveAng() return Math.atan2(dyTotal,dxTotal);

	public inline function distCase(e:Entity) return M.dist(cx+xr, cy+yr, e.cx+e.xr, e.cy+e.yr);
	public inline function distCaseFree(tcx:Int, tcy:Int, ?txr=0.5, ?tyr=0.5) return M.dist(cx+xr, cy+yr, tcx+txr, tcy+tyr);

	public inline function distPx(e:Entity) return M.dist(footX, footY, e.footX, e.footY);
	public inline function distPxFree(x:Float, y:Float) return M.dist(footX, footY, x, y);

	public function makePoint() return new CPoint(cx,cy, xr,yr);

    public inline function destroy() {
        if( !destroyed ) {
            destroyed = true;
            GC.push(this);
        }
    }

    public function dispose() {
        ALL.remove(this);

		colorAdd = null;

		spr.remove();
		spr = null;

		if( debugLabel!=null ) {
			debugLabel.remove();
			debugLabel = null;
		}

		cd.dispose();
		cd = null;

		proto.destroy();
    }

	private function createShape(s : Shape) : tools.Geometry.Shape {
		return
			switch (s) {
				case Circle (r): tools.Geometry.Shape.Circle(centerX, centerY, r);
				case Segment:
					var x1 = centerX;
					var y1 = centerY - properSize;
					var x2 = centerX;
					var y2 = centerY + properSize;
					tools.Geometry.Shape.Segment(x1, y1, x2, y2);
			};
	}

	inline function get_collisionRadius() {
		return
			switch (collisionEShape) {
				case Circle (r): r;
				case Segment: properSize;
			};
	}

	public function checkHit(e:Entity, isEffect) {
		var myShape = if (isEffect) effectShape else collisionShape;
		var eShape = e.collisionShape;

		return tools.Geometry.hasShapeShapeIntersection(myShape, eShape);
	}

	public function checkHitPosition(c : Level.Coord) {
		var cx, cy;
		switch c {
			case Point(x, y):
				cx = x;
				cy = y;
		}

		var shape = effectShape;
		var oShape = tools.Geometry.Shape.Circle((cx+0.5)*Const.GRID, (cy+0.5)*Const.GRID, Const.GRID/2);

		return tools.Geometry.hasShapeShapeIntersection(oShape, shape);
	}

	public inline function debugFloat(v:Float, ?c=0xffffff) {
		debug( pretty(v), c );
	}
	public inline function debug(?v:Dynamic, ?c=0xffffff) {
		#if debug
		if( v==null && debugLabel!=null ) {
			debugLabel.remove();
			debugLabel = null;
		}
		if( v!=null ) {
			if( debugLabel==null )
				debugLabel = new h2d.Text(Assets.fontTiny, Game.ME.scroller);
			debugLabel.text = Std.string(v);
			debugLabel.textColor = c;
		}
		#end
	}

    public function preUpdate() {
		cd.update(tmod);

		oldCx = cx;
		oldCy = cy;

		if(isFixed || isDead)
			cancelVelocities();
    }

    public function postUpdate() {
        spr.x = (cx+xr)*Const.GRID;
        spr.y = (cy+yr)*Const.GRID;
        spr.scaleX = dir*sprScaleX;
        spr.scaleY = sprScaleY;

		if( debugLabel!=null ) {
			debugLabel.x = Std.int(footX - debugLabel.textWidth*0.5);
			debugLabel.y = Std.int(footY+1);
		}

		if (Settings.discrete && isStatic()) {
			xr = 0.5;
			yr = 0.5;
		}
	}



	public function fixedUpdate() {} // runs at a "guaranteed" 30 fps

    public function update() { // runs at an unknown fps
    }

	public function getElementable() : Option<ent.comp.Elementable> {
		return None;
	}

	dynamic public function getCollidable() : Option<ent.comp.Collidable> {
		return None;
	}

	public function teleport(x:Float, y:Float) {
		if(!isTileEntity)
			setPosPixel(x, y);
	}

	public function downlight() {
		for (s in spr.getShaders()) {
			spr.removeShader(s);
		}
		spr.removeChildren();
	}

	public function highlightBase() {
		if (Settings.shaders) {
			var shader = new vfx.shdr.Silhouette(spr.x, spr.y);
			spr.addShader(shader);
		}
		var nspr = new HSprite(spr.lib, spr.groupName, spr);
		nspr.setCenterRatio(0.5, 0.5);
	}

	public function unmark() {
		spr.removeShader(markShdr);
	}

	public function mark() {
		if (Settings.shaders) {
			spr.addShader(markShdr);
		}
	}

	public function getLocatedName() {
		return proto.getLocatedName();
	}
	
	public function log() {
		game.repl.log(proto.getLog());
	}	

	public function addSuffix(s : String) {
		proto.addSuffix(s);
	}

	public function removeSuffix(s : String) {
		proto.removeSuffix(s);
	}

	public function addPrefix(s : String) {
		proto.addPrefix(s);
	}

	public function removePrefix(s : String) {
		proto.removePrefix(s);
	}
}
