/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import dn.Process;
import hxd.Key;

class MapMenu extends dn.Process {
	var ca : dn.legacy.Controller.ControllerAccess;
	var control : tools.CustomControl;
	var mask : h2d.Bitmap;

	var overlay : h2d.Flow;

	public function new() {
		super(Main.ME);

		createRoot(Boot.ME.uiRoot);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering
		var tf = new h2d.Text(Assets.fontLarge, root);
		tf.text = "press Escape to resume";

		ca = Main.ME.controller.createAccess("modal", true);
		control = new tools.CustomControl(ca);

		Game.ME.pause();

		mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, 1, 1, 0.6), root);
		root.under(mask);

		createChildProcess(
				function(c) {
					// Resize dynamically
					tf.setScale( M.imax(1, Math.floor( w()*0.35 / tf.textWidth )) );
					tf.x = Std.int( w()*0.5 - tf.textWidth*tf.scaleX*0.5 );
					tf.y = Std.int( h()*0.1 - tf.textHeight*tf.scaleY*0.5 );

				}, true
				);

		overlay = new h2d.Flow();
		root.add(overlay, 2);

		onResize();

		var xmin = Game.ME.world.all_worlds.Default.levels.length;
		var ymin = Game.ME.world.all_worlds.Default.levels.length;
		var xmax = -Game.ME.world.all_worlds.Default.levels.length;
		var ymax = -Game.ME.world.all_worlds.Default.levels.length;

		var anchor = new h2d.Object();
		root.add(anchor, 3);
		var levels = new Map();

		for (l in Game.ME.world.all_worlds.Default.levels) {
			var x = M.round(l.worldX / l.pxWid);
			var y = M.round(l.worldY / l.pxHei);
			xmin = M.round(Math.min(xmin, x));
			xmax = M.round(Math.max(xmax, x));
			ymin = M.round(Math.min(ymin, y));
			ymax = M.round(Math.max(ymax, y));

			var k = Level.Coord.Point (x, y);
			levels.set(k, l.identifier);
		}

		anchor.x = w()/2-(xmax - xmin + 1) * Const.LEVEL_X/2;
		anchor.y = h()/2-(ymax - ymin + 1) * Const.LEVEL_Y/2;

		var currentLevel = Game.ME.world.all_worlds.Default.levels[Game.ME.curLevelIdx];
		var currentLevelX = M.round(currentLevel.worldX / currentLevel.pxWid);
		var currentLevelY = M.round(currentLevel.worldY / currentLevel.pxHei);

		for (i in xmin...xmax+1) {
			for (j in ymin...ymax+1) {
				var k = Level.Coord.Point (i, j);

				var name = levels.get(k);
				if (name != null) {
					if (Game.ME.visited.exists(k) && (Reflect.field(Assets.mapDict, name) != null)) {
						var g = new HSprite(Assets.map, name, anchor);
						g.x = (i-xmin) * Const.LEVEL_X;
						g.y = (j-ymin) * Const.LEVEL_Y;
					}

					var g = new h2d.Graphics(anchor);
					if (Game.ME.visited.exists(k))
						g.beginFill(0x53E259, 0.);
					else
						g.beginFill(0xffffff, 0.5);

					if (i == currentLevelX && j == currentLevelY) {
						g.lineStyle(1, 0xe2dc53);
					}

					g.drawRect((i-xmin) * Const.LEVEL_X, (j-ymin) * Const.LEVEL_Y, Const.LEVEL_X, Const.LEVEL_Y);
					g.endFill();
				}
			}
		}

		dn.Process.resizeAll();
	}

	override function resume(){
		super.resume();

		ca.takeExclusivity();
	}

	override function onResize() {
		super.onResize();

		root.scale(Const.UI_SCALE);

		mask.scaleX = M.ceil( w()/Const.UI_SCALE );
		mask.scaleY = M.ceil( h()/Const.UI_SCALE );

		overlay.minWidth = overlay.maxWidth = w();
		overlay.minHeight = overlay.maxHeight = h();
	}

	override function onDispose() {
		super.onDispose();
		ca.dispose();
		Game.ME.resume();
	}

	public function close() {
		if( !destroyed ) {
			destroy();
		}
	}

	override function update() {
		super.update();

		if( control.probe(Exit) || control.probe(Map)) {
			close();
		}
	}
}
