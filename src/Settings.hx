/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

class Settings {
	public static var particles = true;
	public static var shakes = true;
	public static var shakePower = 1.0;
	public static var shaders = true;
	public static var speed = 1.0;
	public static var inputSuspend = 0.0;
	public static var assistedAiming = false;
	public static var gameFont = null;
	public static var uiFont = null;
	public static var textHints = true;
	public static var discrete = false;
	public static var sightless = false;

	public static function init() {
		gameFont = Assets.fontPixel;
		uiFont = Assets.fontMedium;

		discrete = sightless;
		assistedAiming = sightless;
	}
}
