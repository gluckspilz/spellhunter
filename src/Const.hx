class Const {
	public static var FPS = 60;
	public static var FIXED_FPS = 30;
	public static var AUTO_SCALE_TARGET_WID = -1; // -1 to disable auto-scaling on width
	public static var AUTO_SCALE_TARGET_HEI = -1; // -1 to disable auto-scaling on height

	public static var LEVEL_X = 24;
	public static var LEVEL_Y = 11;

	/** Viewport scaling **/
	public static var SCALE(get,never) : Int;
	static inline function get_SCALE() {
		// can be replaced with another way to determine the game scaling
		return dn.heaps.Scaler.bestFit_i(LEVEL_X * GRID,LEVEL_Y * GRID);
	}

	/** Specific scaling for top UI elements **/
	public static var UI_SCALE(get,never) : Float;
	static inline function get_UI_SCALE() {
		// can be replaced with another way to determine the UI scaling
		return dn.heaps.Scaler.bestFit_i(LEVEL_X * GRID,LEVEL_Y * GRID);
	}

	public static var GRID = 32;

	static var _uniq = 0;
	public static var NEXT_UNIQ(get,never) : Int; static inline function get_NEXT_UNIQ() return _uniq++;
	public static var INFINITE = 999999;

	static var _inc = 0;
	public static var DP_BG = _inc++;
	public static var DP_FX_BG = _inc++;
	public static var DP_MAIN = _inc++;
	public static var DP_FRONT = _inc++;
	public static var DP_FX_FRONT = _inc++;
	public static var DP_TOP = _inc++;
	public static var DP_UI = _inc++;

	public static var SPELL_OFFSET = 1;
	public static var GRAVITY = .0001;
}
