# Spellhunter, a create your own spell puzzle game.
# Copyright (C) 2021-2023 Quentin Lambert
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

SVGS=$(shell ls res/spell_svg/*.svg)
LIBS=$(subst libs,bin,$(shell ls libs/*))
PNGS=$(subst spell_svg,spell,$(SVGS:.svg=.png))

res/spell:
	$(shell mkdir $@)

res/spell/%.png: res/spell_svg/%.svg res/spell
	inkscape $< --export-background-opacity=0 -w 1080 --export-area-drawing -o $@

bin:
	mkdir bin

$(LIBS): bin
	cp libs/* bin/

js:$(PNGS) $(LIBS)
	haxe js.hxml

js-debug:$(PNGS) $(LIBS)
	haxe js.hxml --debug
